package config

import (
	"testing"
)

func TestConfigLoad(t *testing.T) {

	cnf, err := Load("config.toml")
	if err != nil {
		t.Fail()
		return
	}
	_ = cnf

	if cnf.Bootstrap != "vm0" {
		t.Fail()
	}
	if cnf.Region != "global" {
		t.Fail()
	}
	if cnf.Datacenters[0] != "bootstrap" {
		t.Fail()
	}
	if cnf.Machine["vm0"].Profile != "bootstrap" {
		t.Fail()
	}

}
