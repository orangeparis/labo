package config

import (
	"crypto/rand"
	"encoding/base64"
	"log"
)

/*

	a 32 bit key generator compatible with consul keygen

*/

func ConsulKeygen() (consulKey string, err error) {

	key := make([]byte, 32)
	n, err := rand.Reader.Read(key)
	if err != nil {
		log.Printf("Error reading random data: %s", err)
		return "", err
	}
	if n != 32 {
		log.Printf("Couldn't read enough entropy. Generate more entropy!")
		return "", err
	}
	consulKey = base64.StdEncoding.EncodeToString(key)
	return consulKey, nil

}
