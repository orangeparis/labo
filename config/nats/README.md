
# nats cluster docker


see https://hub.docker.com/_/nats


## deploy nats-main

    $ docker run -d --name nats-main -p 4222:4222 -p 6222:6222 -p 8222:8222 nats
    
    [INF] Starting nats-server version 2.0.4
    [INF] Git commit [c8ca58e]
    [INF] Starting http monitor on 0.0.0.0:8222
    [INF] Listening for client connections on 0.0.0.0:4222
    [INF] Server id is NCEZYE4WBASAAYIPSVWVLU52TXVEN52TDN3KKHWDV33PQ32A6TLBQ7EA
    [INF] Server is ready
    [INF] Listening for route connections on 0.0.0.0:6222 


## deploy nats-2

    $ docker run -d --name=nats-2 --link nats-main -p 4222:4222 -p 6222:6222 -p 8222:8222 nats -c nats-server.conf --routes=nats-route://ruser:T0pS3cr3t@nats-main:6222 -DV
    
    [INF] Starting nats-server version 2.0.4
    [DBG] Go build version go1.12.8
    [INF] Git commit [c8ca58e]
    [INF] Starting http monitor on 0.0.0.0:8222
    [INF] Listening for client connections on 0.0.0.0:4222
    [INF] Server id is NCCJ57DJMZETVT3QIIUQR7MIU5FXCCX7QHTYRDKWNMNYIG43K3Y74J3X
    [INF] Server is ready
    [DBG] Get non local IPs for "0.0.0.0"
    [DBG]  ip=172.17.0.3
    [INF] Listening for route connections on 0.0.0.0:6222
    [DBG] Trying to connect to route on nats-main:6222
    [DBG] 172.17.0.2:6222 - rid:1 - Route connect msg sent
    [INF] 172.17.0.2:6222 - rid:1 - Route connection created
    [DBG] 172.17.0.2:6222 - rid:1 - Registering remote route "NAXUH5CIXFGNRGFWSTZ2DJDJH3Y3X3LDM7RKZCELOUPCKVELMSFWECKS"
    [DBG] 172.17.0.2:6222 - rid:1 - Sent local subscriptions to route
    
    
## deploy nats-3

    $ docker run -d --name=nats-3 --link nats-main -p 4222:4222 -p 6222:6222 -p 8222:8222 nats -c nats-server.conf --routes=nats-route://ruser:T0pS3cr3t@nats-main:6222 -DV
    
    [INF] Starting nats-server version 2.0.4
    [DBG] Go build version go1.12.8
    [INF] Git commit [c8ca58e]
    [INF] Starting http monitor on 0.0.0.0:8222
    [INF] Listening for client connections on 0.0.0.0:4222
    [INF] Server id is NCCJ57DJMZETVT3QIIUQR7MIU5FXCCX7QHTYRDKWNMNYIG43K3Y74J3X
    [INF] Server is ready
    [DBG] Get non local IPs for "0.0.0.0"
    [DBG]  ip=172.17.0.3
    [INF] Listening for route connections on 0.0.0.0:6222
    [DBG] Trying to connect to route on nats-main:6222
    [DBG] 172.17.0.2:6222 - rid:1 - Route connect msg sent
    [INF] 172.17.0.2:6222 - rid:1 - Route connection created
    [DBG] 172.17.0.2:6222 - rid:1 - Registering remote route "NAXUH5CIXFGNRGFWSTZ2DJDJH3Y3X3LDM7RKZCELOUPCKVELMSFWECKS"
    [DBG] 172.17.0.2:6222 - rid:1 - Sent local subscriptions to route
    
    
## nats-server.conf  

default nats configuration


    # Client port of 4222 on all interfaces
    port: 4222
    
    # HTTP monitoring port
    monitor_port: 8222
    
    # This is for clustering multiple servers together.
    cluster {
    
      # Route connections to be received on any interface on port 6222
      port: 6222
    
      # Routes are protected, so need to use them with --routes flag
      # e.g. --routes=nats-route://ruser:T0pS3cr3t@otherdockerhost:6222
      authorization {
        user: ruser
        password: T0pS3cr3t
        timeout: 0.75
      }
    
      # Routes are actively solicited and connected to from this server.
      # This Docker image has none by default, but you can pass a
      # flag to the nats-server docker image to create one to an existing server.
      routes = []
    }
