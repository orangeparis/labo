package consul

import (
	"bitbucket.org/orangeparis/labo/config"
	"encoding/json"
)

//type Configurator interface {
//
//	// get the config : a byte array corresponding to a json configuration
//	GetConfig() ([]byte, error)
//}

// commom to all config , ok for bootstrap
type Base struct {
	Bind             string `json:"bind_addr"`          // 	"bind_addr": "__PRIVATE_IP1__",
	Advertise        string `json:"advertise_addr"`     //	"advertise_addr": "__PRIVATE_IP1__",
	Datacenter       string `json:"datacenter"`         // "datacenter": "dc1",
	DataDir          string `json:"data_dir"`           //	"data_dir": "/var/lib/consul",
	LogLevel         string `json:"log_level"`          // "log_level": "DEBUG",
	EnableSyslog     bool   `json:"enable_syslog"`      // "enable_syslog" : true,
	NodeName         string `json:"node_name"`          //	"node_name": "__NODE_NAME__",
	Encrypt          string `json:"encrypt"`            //	"encrypt": "__ENCRYPTION__",
	RejoinAfterLeave bool   `json:"rejoin_after_leave"` //	"rejoin_after_leave": true

	Bootstrap bool `json:"bootstrap"` //	"bootstrap": false,
	Server    bool `json:"server"`    //	"server": false,
}

func (c Base) GetConfig() ([]byte, error) {
	return json.MarshalIndent(c, "", "  ")
}

// specific for server and client
type Join struct {
	StartJoin     []string `json:"start_join"`     //	"start_join": ["__PRIVATE_IP2__", "__PRIVATE_IP3__"],
	RetryJoin     []string `json:"retry_join"`     //	"retry_join": ["__PRIVATE_IP1__", "__PRIVATE_IP2__", "__PRIVATE_IP3__"],
	RetryInterval string   `json:"retry_interval"` //"retry_interval" : "15s",

}

// specific to server
type Expect struct {
	BootstrapExpect int `json:"bootstrap_expect"` //	 "bootstrap_expect": 3,

}

// a base configuration ( also a bootstrap configuration )
func NewBase(addr string, nodeName string, datacenter string, encrypt string) config.Configurator {

	b := Base{
		Bind:             addr,
		Advertise:        addr,
		Datacenter:       datacenter,
		DataDir:          "/var/lib/consul",
		LogLevel:         "DEBUG",
		EnableSyslog:     true,
		NodeName:         nodeName,
		Encrypt:          encrypt,
		RejoinAfterLeave: true,

		Bootstrap: true,
		Server:    true,
	}
	return b
}

type Client struct {
	Base
	Join
}

func (c Client) GetConfig() ([]byte, error) {
	return json.MarshalIndent(c, "", "  ")
}

func NewClient(addr string, nodeName string, datacenter string, encrypt string,
	startJoin []string, retryJoin []string, RetryInterval string) config.Configurator {

	b := Base{
		Bind:             addr,
		Advertise:        addr,
		Datacenter:       datacenter,
		DataDir:          "/var/lib/consul",
		LogLevel:         "DEBUG",
		EnableSyslog:     true,
		NodeName:         nodeName,
		Encrypt:          encrypt,
		RejoinAfterLeave: true,

		Bootstrap: false,
		Server:    false,
	}
	j := Join{
		StartJoin:     startJoin,
		RetryJoin:     retryJoin,
		RetryInterval: RetryInterval,
	}

	c := Client{b, j}

	return c

}

type Server struct {
	Base
	Join
	Expect
}

func (c Server) GetConfig() ([]byte, error) {
	return json.MarshalIndent(c, "", "  ")
}

func NewServer(addr string, nodeName string, datacenter string, encrypt string,
	startJoin []string, retryJoin []string, RetryInterval string, bootstrapExpect int) config.Configurator {

	b := Base{
		Bind:             addr,
		Advertise:        addr,
		Datacenter:       datacenter,
		DataDir:          "/var/lib/consul",
		LogLevel:         "DEBUG",
		EnableSyslog:     true,
		NodeName:         nodeName,
		Encrypt:          encrypt,
		RejoinAfterLeave: true,

		Bootstrap: false,
		Server:    true,
	}
	j := Join{
		StartJoin:     startJoin,
		RetryJoin:     retryJoin,
		RetryInterval: RetryInterval,
	}
	e := Expect{BootstrapExpect: bootstrapExpect}

	c := Server{b, j, e}

	return c

}
