# consul configuration


## bootstrap

    {
       
        "bind_addr": "__PRIVATE_IP1__",
        "advertise_addr": "__PRIVATE_IP1__",
        "datacenter": "dc1",
        "data_dir": "/var/lib/consul",
        "log_level": "DEBUG",
        "enable_syslog" : true,
        "node_name": "__NODE_NAME__",
        "encrypt": "__ENCRYPTION__",
        "rejoin_after_leave": true
        
         "server": true,
         "bootstrap": true,
    }

## server 

    {
      
        "bind_addr": "__PRIVATE_IP1__",
        "advertise_addr": "__PRIVATE_IP1__",
        "datacenter": "dc1",
        "data_dir": "/var/lib/consul",
        "log_level": "DEBUG",
        "enable_syslog" : true,
        "node_name": "__NODE_NAME__",
        "encrypt": "__ENCRYPTION__",
        "rejoin_after_leave": true
        
        "server": true,
        "bootstrap": false,
        
        "bootstrap_expect": 3,
        "start_join": ["__PRIVATE_IP2__", "__PRIVATE_IP3__"],
        "retry_join": ["__PRIVATE_IP1__", "__PRIVATE_IP2__", "__PRIVATE_IP3__"],
        "retry_interval" : "15s",
       
    }

## client

    {
       
        
        "bind_addr": "__PRIVATE_IP1__",
        "advertise_addr": "__PRIVATE_IP1__",
        "datacenter": "dc1",
        "data_dir": "/var/lib/consul",
        "ui_dir": "/home/consul/dist",
        "log_level": "DEBUG",
        "enable_syslog" : true,
        "node_name": "__NODE_NAME__",
        "encrypt": "__ENCRYPTION__",
         "rejoin_after_leave": true,
         
        
         "bootstrap": false,
         "server": false,
        
        
        "start_join": ["__PRIVATE_IP2__", "__PRIVATE_IP3__", "__PRIVATE_IP4__"],
        "retry_join": ["__PRIVATE_IP2__", "__PRIVATE_IP3__", "__PRIVATE_IP4__"],
        "retry_interval" : "15s",
       
    }
