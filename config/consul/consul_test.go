package consul

import (
	"testing"
)

func TestBootstrap(t *testing.T) {

	c := NewBase("192.168.50.10", "vm0", "bootstrap", "XXX")

	cnf, _ := c.GetConfig()
	print(string(cnf))

}

func TestServer(t *testing.T) {

	startJoin := []string{"192.168.50.10"}
	retryJoin := startJoin
	c := NewServer("192.168.50.11", "vm1", "dc1", "XXX", startJoin, retryJoin, "15s", 3)

	cnf, _ := c.GetConfig()
	print(string(cnf))

}

func TestClient(t *testing.T) {

	startJoin := []string{"192.168.50.10"}
	retryJoin := startJoin
	c := NewClient("192.168.60.11", "vm2", "dc1", "XXX", startJoin, retryJoin, "15s")

	cnf, _ := c.GetConfig()
	print(string(cnf))

}
