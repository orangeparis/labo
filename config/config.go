package config

import (
	"github.com/BurntSushi/toml"
	"io/ioutil"
	"log"
)

var Config *Configuration

type Configurator interface {

	// get the config : a byte array corresponding to a json configuration
	GetConfig() ([]byte, error)
}

//
// defaults
//
var DefaultServices = []string{"consul_bootstrap", "consul_server", "consul_client",
	"nomad_bootstrap", "nomad_server", "nomad_client"}

var DefaultProfiles = []string{"bootstrap", "infra", "worker"}

var DefaultProfileServices = map[string][]string{
	"bootstrap": []string{"consul_bootstrap", "nomad_bootstrap"},
	"infra":     []string{"consul_server", "nomad_server", "nomad_client"},
	"worker":    []string{"consul_client", "nomad_client"},
}

var DefaultLogLevel = "INFO"

//
// Configuration type
//
type Configuration struct {
	Topology
	Parameters Parameters
	Keys       Keys
}

func (c *Configuration) Services() []string {
	return DefaultServices
}
func (c *Configuration) Profiles() []string {
	return DefaultProfiles
}
func (c *Configuration) ProfileServices() map[string][]string {
	return DefaultProfileServices
}

func (c *Configuration) Encrypt() string {
	return "7i+WtYw9q8Sc6eTrIA1iQjHUiEDFnGKKbq9/NtrwKqU="
}

func (c *Configuration) LogLevel() string {
	return DefaultLogLevel
}

// return machine name declared as bootstrap
func (c *Configuration) BootstrapMachineName() string {
	machineName := c.Bootstrap
	if machineName == "" {
		log.Printf("Config: no machine declared as bootstrap\n")
	}
	return machineName
}

// return the ip address of the bootstrap machine
func (c *Configuration) BootstrapIp() string {
	machineName := c.BootstrapMachineName()
	if machineName != "" {
		machineData, ok := Config.Machine[machineName]
		if ok == false {
			log.Printf("Config: No such bootstrap machine: %s\n", machineName)
		}
		ip := machineData.Ip
		if ip != "" {
			return ip
		}
		log.Printf("Config: machine [%s] declared as bootstrap machine has no ip\n", machineName)
	}
	return ""

}

type Machine struct {
	Ip      string //  "192.168.50.10"
	Profile string // "bootstrap"
	Dc      string // "bootstrap"
}

type Topology struct {

	//
	//
	//
	Region    string // "global"
	Bootstrap string // "vm0"

	Datacenters []string //  ["bootstrap","dc1","sniffer"]

	Machine map[string]Machine
}

type Profile struct {
	Services []string
}

type Parameters struct {
	Profiles []string //  ["bootstrap","infra","worker"]
	Services []string // ["consul_bootstrap","consul_server","consul_client","nomad_bootstrap","nomad_server","nomad_client"]

	Profile map[string]Profile
}

type Keys struct {
	ConsulKeygen string
}

func Load(filename string) (cnf *Configuration, err error) {

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Printf("%s", err.Error())
		return
	}

	if _, err := toml.Decode(string(data), &Config); err != nil {
		// handle error
	}

	return Config, err
}
