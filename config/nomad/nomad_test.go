package nomad

import (
	"log"
	"testing"
)

func TestNomadConsulTemplate(t *testing.T) {

	consul := NewConsul("127.0.0.1:8500")

	content, err := consul.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	print(string(content))

}

func TestNomadServerTemplate(t *testing.T) {

	server := Server{
		Enabled:         true,
		BootstrapExpect: 3,
	}

	content, err := server.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	print(string(content))

}

func TestNomadClientTemplate(t *testing.T) {

	client := Client{
		Datacenter: "dc1",
		Enabled:    true,
	}

	content, err := client.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	print(string(content))

}

func TestNomadConfigurationClient(t *testing.T) {

	client := Client{
		Datacenter: "dc1",
		Enabled:    true,
	}

	consul := NewConsul("")

	cnf := NewNomadConfig("192.168.50.11", "dc1", consul, client)

	content, err := cnf.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	print(string(content))

}

func TestNomadConfigurationServer(t *testing.T) {

	server := Server{
		Enabled:         true,
		BootstrapExpect: 3,
	}

	consul := NewConsul("")

	cnf := NewNomadConfig("192.168.50.11", "dc1", consul, server)

	content, err := cnf.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	print(string(content))

}
