package nomad

import (
	"bitbucket.org/orangeparis/labo/config"
	"bytes"
	"text/template"
)

//type Configurator interface {
//
//	// get the config : a byte array corresponding to a json configuration
//	GetConfig() ([]byte, error)
//}

type Base struct {

	// Name the region, if omitted, the default "global" region will be used.
	Region string // region = "europe"

	// Persist data to a location that will survive a machine reboot.
	DataDir string //  data_dir = "/var/lib/nomad/"

	//# Bind to all addresses so that the Nomad agent is available
	//# both on loopback and externally.
	Bind string //bind_addr = "127.0.0.1"

	// Set the bind address
	Addresses map[string]string

	// addresses {
	//	http = "__IP_ADDRESS__"
	//	rpc = "__IP_ADDRESS__"
	//	serf = "__IP_ADDRESS__"
	//}

	// Advertise an accessible IP address so the server is reachable by other servers
	// and clients. The IPs can be materialized by Terraform or be replaced by an
	//init script.
	Advertise map[string]string

	//	advertise {
	//	http = "__IP_ADDRESS__:4646"
	//	rpc = "__IP_ADDRESS__:4647"
	//	serf = "__IP_ADDRESS__:4648"
	// }

	// Network ports can be also set. The default values are:
	Ports map[string]int
	//	# ports {
	//	#   http = 4646
	//	#   rpc = 4647  # only server nodes
	//	#   serf = 4648 # only server nodes
	//	# }

	//# Log verbosity (INFO, DEBUG)
	LogLevel string // log_level = "DEBUG"

	/*
		# Ship metrics to monitor the health of the cluster and
		# to see task resource usage.
		#telemetry {
		#    statsite_address = "${var.statsite}"
		#    disable_hostname = true
		#}
	*/

	// Enable debug endpoints.
	EnableDebug bool // enable_debug = true

	// Consul Configuration
	Consul string

	// ClientOrServer
	ClientOrServer string
}

func (c Base) GetConfig() ([]byte, error) {

	pattern := `
# Name the region, if omitted, the default "global" region will be used.
region = "{{.Region}}"

# Persist data to a location that will survive a machine reboot.
data_dir = "{{.DataDir}}"

# Bind to all addresses so that the Nomad agent is available 
# both on loopback and externally.
bind_addr = "{{.Bind}}""

# Set the bind address
addresses {
	http = "{{.Addresses.http}}"
	rpc = "{{.Addresses.rpc}}"
	serf = "{{.Addresses.serf}}"
}

advertise {
	http = "{{.Advertise.http}}"
	rpc = "{{.Advertise.rpc}}"
	serf = "{{.Advertise.serf}}"
}

# Network ports can be also set. The default values are:
ports {
  	http = {{.Ports.http}}
	# rpc = 4647  # only server nodes
	# serf = 4648 # only server nodes
}

# Log verbosity (INFO, DEBUG)
log_level = "{{.LogLevel}}"

# Enable debug endpoints.
enable_debug = {{.EnableDebug}}

{{.Consul}}

{{.ClientOrServer}}

`
	text := ""
	tmpl, err := template.New("nomad").Parse(pattern)
	if err != nil {
		return []byte{}, err
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, c)
	return buf.Bytes(), err
}

type Consul struct {
	// Consul Configuration for nomad

	// consul {

	// # Consult agent's HTTP Address
	Address string //	address = "127.0.0.1:8500"

	// The service name to register the server and client with Consul.
	ServerServiceName string //	server_service_name = "nomad"
	ClientServiceName string //	client_service_name = "nomad-client"

	// Auth info for http access
	Auth string //	auth = user:password

	// Advertise Nomad services to Consul
	// Enables automatically registering the services
	AutoAdvertise bool //	auto_advertise = true

	// Enables the servers and clients bootstrap using Consul
	ServerAutoJoin bool // server_auto_join = true
	ClientAutoJoin bool // client_auto_join = true
	//	}

}

func (c Consul) GetConfig() ([]byte, error) {

	pattern := `
consul {
	# Consult agent's HTTP Address
	address = "{{.Address}}"
	
	# The service name to register the server and client with Consul.
	server_service_name = "{{.ServerServiceName}}"
	client_service_name = "{{.ClientServiceName}}"
	
	# Auth info for http access
	#auth = user:password
	
	# Advertise Nomad services to Consul
	# Enables automatically registering the services
	auto_advertise = {{.AutoAdvertise}}
	
	# Enables the servers and clients bootstrap using Consul
	server_auto_join = {{.ServerAutoJoin}}
	client_auto_join = {{.ClientAutoJoin}}
}
`
	text := ""
	tmpl, err := template.New("consul").Parse(pattern)
	if err != nil {
		return []byte{}, err
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, c)
	return buf.Bytes(), err
}

type Server struct {

	// ----------- Server Configuration -----------

	// server {
	// # Enable server mode for the local agent
	Enabled bool //  enabled = true

	// Number of server nodes to wait for before
	// bootstrapping, depending on cluster size
	BootstrapExpect int //bootstrap_expect = 3
	// }
}

func (c Server) GetConfig() ([]byte, error) {

	pattern := `
server {
	# Enable server mode for the local agent
	enabled = {{.Enabled}}

	{{ if ne .BootstrapExpect 0 }}
	# Number of server nodes to wait for before
	# bootstrapping, depending on cluster size
	bootstrap_expect = {{.BootstrapExpect}}
	{{ end }}
}
`
	text := ""
	tmpl, err := template.New("server").Parse(pattern)
	if err != nil {
		return []byte{}, err
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, c)
	return buf.Bytes(), err
}

type Client struct {
	// ----------- Client Configuration -----------

	//Datacenter to register the client
	Datacenter string //  datacenter = "dc1"

	// client {
	//# Enable client mode for the local agent
	Enabled bool // enabled = true
	/*
		# Reserve a portion of the nodes resources
		# from being used by Nomad when placing tasks.
		# For example:
		# reserved {
		#     cpu = 500 # MHz
		#     memory = 512 # MB
		#     disk = 1024 # MB
		#     reserved_ports = "22,80,8500-8600"
	*/

	//}
}

func (c Client) GetConfig() ([]byte, error) {

	pattern := `
# Datacenter to register the client
datacenter = "{{.Datacenter}}"

client {
	# Enable client mode for the local agent
	enabled = {{.Enabled}}
	
	# Reserve a portion of the nodes resources
	# from being used by Nomad when placing tasks.
	# For example:
	# reserved {
	#     cpu = 500 # MHz
	#     memory = 512 # MB
	#     disk = 1024 # MB
	#     reserved_ports = "22,80,8500-8600"
	# }
}
`
	text := ""
	tmpl, err := template.New("client").Parse(pattern)
	if err != nil {
		return []byte{}, err
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, c)
	return buf.Bytes(), err
}

// create consul configuration for nomad
func NewConsul(address string) config.Configurator {
	// Address string		//	address = "127.0.0.1:8500"
	if address == "" {
		address = "127.0.0.1:8500"
	}

	c := Consul{
		Address:           address,
		ServerServiceName: "nomad",
		ClientServiceName: "nomad-client",
		Auth:              "user:password",
		AutoAdvertise:     true,
		ServerAutoJoin:    true,
		ClientAutoJoin:    true,
	}
	return c
}

func NewNomadConfig(address string, region string, consul config.Configurator, clientOrServer config.Configurator) config.Configurator {

	cnf := Base{
		Region:  region,
		DataDir: "/var/lib/nomad/",
		//# Bind to all addresses so that the Nomad agent is available
		//# both on loopback and externally.
		Bind:        "127.0.0.1",
		LogLevel:    "DEBUG",
		EnableDebug: true,
	}
	cnf.Addresses = map[string]string{
		"http": address,
		"rpc":  address,
		"serf": address}

	cnf.Advertise = map[string]string{
		"http": address + ":4646",
		"rpc":  address + ":4647",
		"serf": address + ":4648"}

	cnf.Ports = map[string]int{
		"http": 4646,
		//	#   http = 4646
		//	#   rpc = 4647  # only server nodes
		//	#   serf = 4648 # only server nodes
	}

	if x, err := consul.GetConfig(); err == nil {
		cnf.Consul = string(x)
	}
	if y, err := clientOrServer.GetConfig(); err == nil {
		cnf.ClientOrServer = string(y)
	}
	return cnf
}
