package gen

import (
	"bitbucket.org/orangeparis/labo/config"
	"bitbucket.org/orangeparis/labo/config/consul"
	"bitbucket.org/orangeparis/labo/config/nomad"
	"errors"
	"log"
)

func GenConf(machine string) (configs map[string]string, err error) {

	configs = make(map[string]string)

	encrypt := config.Config.Encrypt()

	bootstrapIp := config.Config.BootstrapIp()
	if bootstrapIp == "" {
		err = errors.New("Genconf: No bootstrap ip")
		return configs, err
	}

	log.Printf("Genconf: for machine [%s]\n", machine)

	machineData, ok := config.Config.Machine[machine]
	if ok == false {
		log.Printf("No such machine: %s\n", machine)
		return configs, err
	}
	profile := machineData.Profile
	// determine services for this profile
	serviceList := config.Config.ProfileServices()
	services, ok := serviceList[profile]
	if ok != true {
		log.Printf("GenConf [%s] cannot fetch services for this profile: %s\n", machine, profile)
		return configs, err
	}
	log.Printf("GenConf [%s]: profile is [%s]\n", machine, profile)

	for _, service := range services {

		log.Printf("GenConf [%s]: generate configuration for service [%s]\n", machine, service)

		switch service {

		//
		// consul configurations
		//
		case "consul_bootstrap":
			cnf, err := ConsulBootstrapConfiguration(machine, machineData, encrypt)
			if err != nil {
				log.Printf("GenConf [%s]: failed to generate service %s : %s", machine, service, err.Error())
				return configs, err
			}
			configs[service] = string(cnf)

		case "consul_server":
			cnf, err := ConsulServerConfiguration(machine, machineData, encrypt, bootstrapIp)
			if err != nil {
				log.Printf("GenConf [%s]: failed to generate service %s : %s\n", machine, service, err.Error())
				return configs, err
			}
			configs[service] = string(cnf)

		case "consul_client":
			cnf, err := ConsulClientConfiguration(machine, machineData, encrypt, bootstrapIp)
			if err != nil {
				log.Printf("GenConf [%s]: failed to generate service %s : %s\n", machine, service, err.Error())
				return configs, err
			}
			configs[service] = string(cnf)

		//
		// nomad configurations
		//
		case "nomad_bootstrap":
			cnf, err := NomadBootstrapConfiguration(machine, machineData, encrypt)
			if err != nil {
				log.Printf("GenConf [%s]: failed to generate service %s : %s", machine, service, err.Error())
				return configs, err
			}
			configs[service] = string(cnf)

		case "nomad_server":
			cnf, err := NomadServerConfiguration(machine, machineData, encrypt)
			if err != nil {
				log.Printf("GenConf [%s]: failed to generate service %s : %s", machine, service, err.Error())
				return configs, err
			}
			configs[service] = string(cnf)

		case "nomad_client":
			cnf, err := NomadClientConfiguration(machine, machineData, encrypt)
			if err != nil {
				log.Printf("GenConf [%s]: failed to generate service %s : %s", machine, service, err.Error())
				return configs, err
			}
			configs[service] = string(cnf)

		default:
			log.Printf("GenConf [%s]: service not implemented: %s", machine, service)
			return
		}

	}
	log.Printf("GenConf [%s] configuration generated\n", machine)
	return configs, err
}

//
// consul configuration
//

func ConsulBootstrapConfiguration(name string, machine config.Machine, encrypt string) (cnf []byte, err error) {

	c := consul.NewBase(machine.Ip, name, machine.Profile, encrypt)

	cnf, _ = c.GetConfig()
	println(string(cnf))

	return cnf, err
}

func ConsulServerConfiguration(name string, machine config.Machine, encrypt string, bootstrapIp string) (cnf []byte, err error) {

	startJoin := []string{bootstrapIp}
	retryJoin := startJoin
	c := consul.NewServer(machine.Ip, name, machine.Dc, encrypt, startJoin, retryJoin, "15s", 3)

	s, _ := c.GetConfig()
	println(string(s))

	return s, err
}

func ConsulClientConfiguration(name string, machine config.Machine, encrypt string, bootstrapIp string) (cnf []byte, err error) {

	startJoin := []string{bootstrapIp}
	retryJoin := startJoin
	c := consul.NewClient(machine.Ip, name, machine.Dc, encrypt, startJoin, retryJoin, "15s")

	s, _ := c.GetConfig()
	println(string(s))

	return s, err
}

//
// nomad configuration
//
func NomadBootstrapConfiguration(name string, machine config.Machine, encrypt string) (cnf []byte, err error) {

	server := nomad.Server{
		Enabled:         true,
		BootstrapExpect: 3,
	}

	nomadConsulConfiguration := nomad.NewConsul("")

	s := nomad.NewNomadConfig(machine.Ip, machine.Dc, nomadConsulConfiguration, server)

	content, err := s.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	println(string(content))
	return content, err
}

func NomadServerConfiguration(name string, machine config.Machine, encrypt string) (cnf []byte, err error) {

	server := nomad.Server{
		Enabled:         true,
		BootstrapExpect: 3,
	}

	nomadConsulConfiguration := nomad.NewConsul("")

	s := nomad.NewNomadConfig(machine.Ip, machine.Dc, nomadConsulConfiguration, server)

	content, err := s.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	println(string(content))
	return content, err
}

func NomadClientConfiguration(name string, machine config.Machine, encrypt string) (cnf []byte, err error) {

	client := nomad.Client{
		Datacenter: machine.Dc,
		Enabled:    true,
	}

	nomadConsulConfiguration := nomad.NewConsul("")

	s := nomad.NewNomadConfig(machine.Ip, machine.Dc, nomadConsulConfiguration, client)
	content, err := s.GetConfig()
	if err != nil {
		log.Printf("%s", err.Error())
	}
	println(string(content))
	return content, err
}
