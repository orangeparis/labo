package gen

import (
	"bitbucket.org/orangeparis/labo/config"
	"errors"
)

//
func GetServiceFilename(service string) (string, error) {

	var err error

	services := config.Config.Services()
	_ = services

	switch service {

	case "consul_bootstrap":
		return "consul_server.json", nil
	case "consul_server":
		return "consul_server.json", nil
	case "consul_client":
		return "consul_client.json", nil
	case "nomad_bootstrap":
		return "nomad_server.hcl", nil
	case "nomad_server":
		return "nomad_server.hcl", nil
	case "nomad_client":
		return "nomad_client.hcl", nil

	default:
		err = errors.New("service unknown")
		return "", err
	}

}
