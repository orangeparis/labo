package gen

import (
	"bitbucket.org/orangeparis/labo/config"
	"log"
	"testing"
)

func TestGenConfigBootstrap(t *testing.T) {

	config.Load("../config.toml")

	configs, err := GenConf("vm0")
	if err != nil {
		log.Printf("%s\n", err.Error())
	}

	print(configs)

}

func TestGenConfigInfra(t *testing.T) {

	config.Load("../config.toml")

	configs, err := GenConf("vm1")
	if err != nil {
		log.Printf("%s\n", err.Error())
	}

	print(configs)

}

func TestGenConfigWorker(t *testing.T) {

	config.Load("../config.toml")

	configs, err := GenConf("vm3")
	if err != nil {
		log.Printf("%s\n", err.Error())
	}

	print(configs)

}
