package _systemd_test

import (
	"bitbucket.org/orangeparis/labo/systemd"
	"testing"
)

func TestNomadSystemd(t *testing.T) {

	sv, err := _systemd.NewSystemdService("nomad", "nomad cluster", "consul",
		"/usr/bin/nomad", "agent --config /etc/nomad.d/config.json")

	_ = err
	_ = sv

	return

}
