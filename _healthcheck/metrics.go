package _healthcheck

import (
	"github.com/prometheus/client_golang/prometheus"
)

// InitMetrics : declare metrics
func InitMetrics() {

	prometheus.MustRegister(Uptime)

}

// Uptime : uptime in seconds of the source
var Uptime = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "uptime",
		Help: "count of seconds since started",
	},
	[]string{"device", "name"}, // eg device=sniffer , name=S1
)

//
// metrics recorders ( helpers to reccord metrics)
//

// RecordUptime : number of seconds since started
func RecordUptime(count float64, device string, source string) {
	Uptime.With(prometheus.Labels{"device": device, "source": source}).Set(count)
}
