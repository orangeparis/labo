package _healthcheck

import (
	"context"
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
	"strings"
)

/*
	scanner

	subscribe to event.*.*.uptime

	and update the heartbeat


*/

var topic = "event.*.*.uptime"

func Scan(ctx context.Context, natsUrl string, heartbeater Heartbeater) error {

	// create nats connection
	nc, err := nats.Connect(natsUrl)
	if err != nil {
		log.Printf("cannot open nats connection: %s", err.Error())
		return err
	}
	defer nc.Close()
	msg := fmt.Sprintf("Healtcheck scanner connected with nats at : %s\n", natsUrl)
	log.Println(msg)

	// subscribe to all inputs
	_, err = nc.Subscribe(topic, func(m *nats.Msg) {
		log.Printf("healthcheck scanner caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
		HandleMessage(heartbeater, m.Subject, m.Data)

	})
	if err != nil {
		log.Printf("")
	}

	// wait for cancel
	for {
		log.Printf("healthcheck scanner enter loop (wait for cancel)")
		select {
		case <-ctx.Done(): // Done  this context is canceled -> exit
			log.Println("healthcheck scanner Exiting")
			return err
		}
	}
}

// handle uptime message with subject event.*.*.uptime  and body {"count":int}
func HandleMessage(heartbeater Heartbeater, subject string, message []byte) {

	// split subject to extract service name
	parts := strings.Split(subject, ".")
	if len(parts) != 4 {
		// should not be there
		log.Printf("Healthcheck scanner : bad subject:%s\n", subject)
		return
	}
	// eg sniffer.1
	service := parts[1] + "." + parts[2]

	// set timestamp for this service
	log.Printf("Healthcheck scanner set heartbeat for service : %s\n", service)
	_ = heartbeater.Set(service)

	// create prometheus metric ?

}
