# Healthcheck


a process and an api to check health services using nats

## prerequisites

each service when started must emit an "uptime message" every 5 seconds

the subject is event.<name>.<instance>.uptime 
the body is { "count": int } , number of seconds since started

eg the service sniffer.1 emits every 5 seconds to event.sniffer.uptime


## the process

the process subscribes to event.*.*.uptime 
and store each count in a map   "sniffer.1" : 123

## the api exposes an url 

    GET /healthcheck/<name.instancence>    eg /healthcheck/sniffer.1
    
    return 200 if OK
    return 404 if not
    
    
 