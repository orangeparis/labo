package _healthcheck

var healthchecker Heartbeater

type Heartbeater interface {
	Set(name string) error    // record a timestamp for this name
	IsAlive(name string) bool // check a timestamp for this name
}
