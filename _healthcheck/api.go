package _healthcheck

import (
	"context"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io"
	"net/http"
	"strings"
)

var healthCheckRoute = "/health-check/"

func Route(heartbeat Heartbeater) {

	// register route for /health-check/*
	http.Handle(healthCheckRoute, healthcheckHandler(heartbeat))

	// register route for /metrics
	http.Handle("/metrics", promhttp.Handler())

}

// a closure with checker
func healthcheckHandler(checker Heartbeater) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// parse the path /health-check/sniffer.1
		var serviceName = ""
		parts := strings.Split(r.URL.String(), "/")
		if len(parts) >= 2 {
			serviceName = parts[2]
		}
		if serviceName == "" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		// check service heartbeat
		alive := checker.IsAlive(serviceName)
		if alive != true {
			w.WriteHeader(http.StatusRequestTimeout)
			return
		}

		// A very simple health check.
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		// In the future we could report back on the status of our DB, or our cache
		// (e.g. Redis) by performing a simple PING, and include them in the response.
		io.WriteString(w, `{"alive": true}`)

	})
}

func Setup(natsServer string, delay int) {

	// create a heartbeater
	heartbeat := NewMapHeartBeater(delay)

	// create route for health-check/
	Route(heartbeat)

	// create and launch the scanner process  subscribes to event.*.*.uptime
	ctx := context.Background()
	go Scan(ctx, natsServer, heartbeat)

	// init prometheus metrics
	InitMetrics()

}
