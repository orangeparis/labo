package _healthcheck

import (
	"sync"
	"time"
)

/*
	an implementation of an uptimer based on a map[string]int


*/

type MapHeartbeater struct {
	mux   sync.Mutex
	Map   map[string]int // the map to store the uptime  "sniffer.1" : 12
	Delay int            // the delay in seconds to evaluate if service is alive eg 6 s

}

// implements Uptimer interface
func (u MapHeartbeater) Set(name string) error {
	u.mux.Lock()
	u.Map[name] = int(time.Now().Unix())
	u.mux.Unlock()
	return nil
}

// check if service is alive  ( eg name = "sniffer.1" )
func (u MapHeartbeater) IsAlive(name string) bool {

	u.mux.Lock()
	last, ok := u.Map[name]
	u.mux.Unlock()
	if ok == false {
		// we dont find this service in the map
		return false
	}
	// we found a timestamp for this service
	now := int(time.Now().Unix())
	if now-last <= u.Delay {
		// we are good
		return true
	}
	// the time stamp is obsolete
	return false
}

func NewMapHeartBeater(delay int) MapHeartbeater {
	if delay == 0 {
		delay = 10
	}
	u := MapHeartbeater{Map: make(map[string]int), Delay: delay}
	return u
}
