

# start vm2

    vagrant@vm2:/vagrant$ sudo consul agent --config-dir vm2-consul_server.json
    WARNING: LAN keyring exists but -encrypt given, using keyring
    WARNING: WAN keyring exists but -encrypt given, using keyring
    bootstrap_expect > 0: expecting 3 servers
    ==> Starting Consul agent...
               Version: 'v1.6.2'
               Node ID: '1601af2d-875f-a40f-9830-813eea971b3d'
             Node name: 'vm2'
            Datacenter: 'dc1' (Segment: '<all>')
                Server: true (Bootstrap: false)
           Client Addr: [127.0.0.1] (HTTP: 8500, HTTPS: -1, gRPC: -1, DNS: 8600)
          Cluster Addr: 192.168.50.12 (LAN: 8301, WAN: 8302)
               Encrypt: Gossip: true, TLS-Outgoing: false, TLS-Incoming: false, Auto-Encrypt-TLS: false
    
    ==> Log data will now stream in as it occurs:
    
        2019/12/13 12:35:59 [DEBUG] tlsutil: Update with version 1
        2019/12/13 12:35:59 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
        2019/12/13 12:35:59 [INFO]  raft: Initial configuration (index=0): []
        2019/12/13 12:35:59 [INFO] serf: EventMemberJoin: vm2.dc1 192.168.50.12
        2019/12/13 12:35:59 [INFO] serf: EventMemberJoin: vm2 192.168.50.12
        2019/12/13 12:35:59 [INFO] agent: Started DNS server 127.0.0.1:8600 (udp)
        2019/12/13 12:35:59 [INFO]  raft: Node at 192.168.50.12:8300 [Follower] entering Follower state (Leader: "")
        2019/12/13 12:35:59 [WARN] serf: Failed to re-join any previously known node
        2019/12/13 12:35:59 [WARN] serf: Failed to re-join any previously known node
        2019/12/13 12:35:59 [INFO] consul: Adding LAN server vm2 (Addr: tcp/192.168.50.12:8300) (DC: dc1)
        2019/12/13 12:35:59 [INFO] consul: Handled member-join event for server "vm2.dc1" in area "wan"
        2019/12/13 12:35:59 [INFO] agent: Started DNS server 127.0.0.1:8600 (tcp)
        2019/12/13 12:35:59 [INFO] agent: Started HTTP server on 127.0.0.1:8500 (tcp)
        2019/12/13 12:35:59 [INFO] agent: started state syncer
    ==> Consul agent running!
        2019/12/13 12:35:59 [INFO] agent: Retry join LAN is supported for: aliyun aws azure digitalocean gce k8s mdns os packet scaleway softlayer triton vsphere
        2019/12/13 12:35:59 [INFO] agent: Joining LAN cluster...
        2019/12/13 12:35:59 [INFO] agent: (LAN) joining: [192.168.50.10]
        2019/12/13 12:35:59 [DEBUG] memberlist: Failed to join 192.168.50.10: dial tcp 192.168.50.10:8301: connect: connection refused
        2019/12/13 12:35:59 [WARN] agent: (LAN) couldn't join: 0 Err: 1 error occurred:
    	* Failed to join 192.168.50.10: dial tcp 192.168.50.10:8301: connect: connection refused
    
        2019/12/13 12:35:59 [WARN] agent: Join LAN failed: <nil>, retrying in 15s
        2019/12/13 12:36:06 [ERR] agent: failed to sync remote state: No cluster leader
        2019/12/13 12:36:09 [WARN]  raft: no known peers, aborting election
        2019/12/13 12:36:14 [INFO] agent: (LAN) joining: [192.168.50.10]
        2019/12/13 12:36:14 [DEBUG] memberlist: Failed to join 192.168.50.10: dial tcp 192.168.50.10:8301: connect: connection refused
        2019/12/13 12:36:14 [WARN] agent: (LAN) couldn't join: 0 Err: 1 error occurred:
    	* Failed to join 192.168.50.10: dial tcp 192.168.50.10:8301: connect: connection refused
    
        2019/12/13 12:36:14 [WARN] agent: Join LAN failed: <nil>, retrying in 15s
        2019/12/13 12:36:29 [INFO] agent: (LAN) joining: [192.168.50.10]
        2019/12/13 12:36:29 [DEBUG] memberlist: Failed to join 192.168.50.10: dial tcp 192.168.50.10:8301: connect: connection refused
        2019/12/13 12:36:29 [WARN] agent: (LAN) couldn't join: 0 Err: 1 error occurred:
    	* Failed to join 192.168.50.10: dial tcp 192.168.50.10:8301: connect: connection refused
    
        2019/12/13 12:36:29 [WARN] agent: Join LAN failed: <nil>, retrying in 15s
        2019/12/13 12:36:32 [ERR] agent: Coordinate update error: No cluster leader
        2019/12/13 12:36:34 [ERR] agent: failed to sync remote state: No cluster leader


# start vm0 ( bootstrap )

    vagrant@vm0:/vagrant$ sudo consul agent --config-dir vm0-consul_server.json
    bootstrap = true: do not enable unless necessary
    ==> Starting Consul agent...
               Version: 'v1.6.2'
               Node ID: '14355616-ca8d-adfa-fc89-3f505b1db70b'
             Node name: 'vm0'
            Datacenter: 'dc1' (Segment: '<all>')
                Server: true (Bootstrap: true)
           Client Addr: [127.0.0.1] (HTTP: 8500, HTTPS: -1, gRPC: -1, DNS: 8600)
          Cluster Addr: 192.168.50.10 (LAN: 8301, WAN: 8302)
               Encrypt: Gossip: true, TLS-Outgoing: false, TLS-Incoming: false, Auto-Encrypt-TLS: false
    
    ==> Log data will now stream in as it occurs:
    
        2019/12/13 12:37:46 [DEBUG] agent: Using random ID "14355616-ca8d-adfa-fc89-3f505b1db70b" as node ID
        2019/12/13 12:37:46 [DEBUG] tlsutil: Update with version 1
        2019/12/13 12:37:46 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
        2019/12/13 12:37:47 [INFO]  raft: Initial configuration (index=1): [{Suffrage:Voter ID:14355616-ca8d-adfa-fc89-3f505b1db70b Address:192.168.50.10:8300}]
        2019/12/13 12:37:47 [INFO] serf: EventMemberJoin: vm0.dc1 192.168.50.10
        2019/12/13 12:37:47 [INFO] serf: EventMemberJoin: vm0 192.168.50.10
        2019/12/13 12:37:47 [INFO]  raft: Node at 192.168.50.10:8300 [Follower] entering Follower state (Leader: "")
        2019/12/13 12:37:47 [INFO] consul: Adding LAN server vm0 (Addr: tcp/192.168.50.10:8300) (DC: dc1)
        2019/12/13 12:37:47 [INFO] consul: Handled member-join event for server "vm0.dc1" in area "wan"
        2019/12/13 12:37:47 [INFO] agent: Started DNS server 127.0.0.1:8600 (tcp)
        2019/12/13 12:37:47 [INFO] agent: Started DNS server 127.0.0.1:8600 (udp)
        2019/12/13 12:37:47 [INFO] agent: Started HTTP server on 127.0.0.1:8500 (tcp)
        2019/12/13 12:37:47 [INFO] agent: started state syncer
    ==> Consul agent running!
        2019/12/13 12:37:54 [ERR] agent: failed to sync remote state: No cluster leader
        2019/12/13 12:37:56 [WARN]  raft: Heartbeat timeout from "" reached, starting election
        2019/12/13 12:37:56 [INFO]  raft: Node at 192.168.50.10:8300 [Candidate] entering Candidate state in term 2
        2019/12/13 12:37:56 [DEBUG] raft: Votes needed: 1
        2019/12/13 12:37:56 [DEBUG] raft: Vote granted from 14355616-ca8d-adfa-fc89-3f505b1db70b in term 2. Tally: 1
        2019/12/13 12:37:56 [INFO]  raft: Election won. Tally: 1
        2019/12/13 12:37:56 [INFO]  raft: Node at 192.168.50.10:8300 [Leader] entering Leader state
        2019/12/13 12:37:56 [INFO] consul: cluster leadership acquired
        2019/12/13 12:37:56 [INFO] consul: New leader elected: vm0
        2019/12/13 12:37:56 [DEBUG] consul: Skipping self join check for "vm0" since the cluster is too small
        2019/12/13 12:37:56 [INFO] consul: member 'vm0' joined, marking health alive
        2019/12/13 12:37:58 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
        2019/12/13 12:37:59 [DEBUG] agent: Skipping remote check "serfHealth" since it is managed automatically
        2019/12/13 12:37:59 [INFO] agent: Synced node info
        2019/12/13 12:37:59 [DEBUG] agent: Node info in sync
        2019/12/13 12:37:59 [DEBUG] memberlist: Stream connection from=192.168.50.12:60090
        2019/12/13 12:37:59 [INFO] serf: EventMemberJoin: vm2 192.168.50.12
        2019/12/13 12:37:59 [INFO] consul: Adding LAN server vm2 (Addr: tcp/192.168.50.12:8300) (DC: dc1)
        2019/12/13 12:37:59 [INFO]  raft: Updating configuration with AddNonvoter (1601af2d-875f-a40f-9830-813eea971b3d, 192.168.50.12:8300) to [{Suffrage:Voter ID:14355616-ca8d-adfa-fc89-3f505b1db70b Address:192.168.50.10:8300} {Suffrage:Nonvoter ID:1601af2d-875f-a40f-9830-813eea971b3d Address:192.168.50.12:8300}]
        2019/12/13 12:37:59 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8302
        2019/12/13 12:37:59 [INFO]  raft: Added peer 1601af2d-875f-a40f-9830-813eea971b3d, starting replication
        2019/12/13 12:37:59 [INFO] consul: member 'vm2' joined, marking health alive
        2019/12/13 12:37:59 [INFO] serf: EventMemberJoin: vm2.dc1 192.168.50.12
        2019/12/13 12:37:59 [DEBUG] consul: Successfully performed flood-join for "vm2" at 192.168.50.12:8302
        2019/12/13 12:37:59 [INFO] consul: Handled member-join event for server "vm2.dc1" in area "wan"
        2019/12/13 12:37:59 [WARN]  raft: AppendEntries to {Nonvoter 1601af2d-875f-a40f-9830-813eea971b3d 192.168.50.12:8300} rejected, sending older logs (next: 1)
        2019/12/13 12:37:59 [INFO]  raft: pipelining replication to peer {Nonvoter 1601af2d-875f-a40f-9830-813eea971b3d 192.168.50.12:8300}
        2019/12/13 12:37:59 [DEBUG] serf: messageJoinType: vm2
        2019/12/13 12:37:59 [DEBUG] serf: messageJoinType: vm2
        2019/12/13 12:38:00 [DEBUG] serf: messageJoinType: vm2
        2019/12/13 12:38:00 [DEBUG] serf: messageJoinType: vm0.dc1
        2019/12/13 12:38:00 [DEBUG] serf: messageJoinType: vm2
        2019/12/13 12:38:00 [DEBUG] serf: messageJoinType: vm0.dc1
        2019/12/13 12:38:00 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
        2019/12/13 12:38:01 [DEBUG] serf: messageJoinType: vm0.dc1
        2019/12/13 12:38:01 [DEBUG] serf: messageJoinType: vm0.dc1
        2019/12/13 12:38:04 [DEBUG] memberlist: Stream connection from=192.168.50.12:46026
        2019/12/13 12:38:16 [INFO] autopilot: Promoting Server (ID: "1601af2d-875f-a40f-9830-813eea971b3d" Address: "192.168.50.12:8300") to voter
        2019/12/13 12:38:16 [INFO]  raft: Updating configuration with AddStaging (1601af2d-875f-a40f-9830-813eea971b3d, 192.168.50.12:8300) to [{Suffrage:Voter ID:14355616-ca8d-adfa-fc89-3f505b1db70b Address:192.168.50.10:8300} {Suffrage:Voter ID:1601af2d-875f-a40f-9830-813eea971b3d Address:192.168.50.12:8300}]
        
        
# start vm1 
     
        vagrant@vm1:/vagrant$ sudo consul agent --config-dir vm1-consul_server.json
        bootstrap_expect > 0: expecting 3 servers
        ==> Starting Consul agent...
                   Version: 'v1.6.2'
                   Node ID: 'd4a52cdc-ec02-1653-16e9-600b9b65d5f5'
                 Node name: 'vm1'
                Datacenter: 'dc1' (Segment: '<all>')
                    Server: true (Bootstrap: false)
               Client Addr: [127.0.0.1] (HTTP: 8500, HTTPS: -1, gRPC: -1, DNS: 8600)
              Cluster Addr: 192.168.50.11 (LAN: 8301, WAN: 8302)
                   Encrypt: Gossip: true, TLS-Outgoing: false, TLS-Incoming: false, Auto-Encrypt-TLS: false
        
        ==> Log data will now stream in as it occurs:
        
            2019/12/13 12:39:53 [DEBUG] agent: Using random ID "d4a52cdc-ec02-1653-16e9-600b9b65d5f5" as node ID
            2019/12/13 12:39:53 [DEBUG] tlsutil: Update with version 1
            2019/12/13 12:39:53 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
            2019/12/13 12:39:53 [INFO]  raft: Initial configuration (index=0): []
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm1.dc1 192.168.50.11
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm1 192.168.50.11
            2019/12/13 12:39:53 [INFO] agent: Started DNS server 127.0.0.1:8600 (udp)
            2019/12/13 12:39:53 [INFO]  raft: Node at 192.168.50.11:8300 [Follower] entering Follower state (Leader: "")
            2019/12/13 12:39:53 [INFO] consul: Adding LAN server vm1 (Addr: tcp/192.168.50.11:8300) (DC: dc1)
            2019/12/13 12:39:53 [INFO] consul: Handled member-join event for server "vm1.dc1" in area "wan"
            2019/12/13 12:39:53 [INFO] agent: Started DNS server 127.0.0.1:8600 (tcp)
            2019/12/13 12:39:53 [INFO] agent: Started HTTP server on 127.0.0.1:8500 (tcp)
        ==> Joining cluster...
            2019/12/13 12:39:53 [INFO] agent: (LAN) joining: [192.168.50.10]
            2019/12/13 12:39:53 [INFO] agent: Retry join LAN is supported for: aliyun aws azure digitalocean gce k8s mdns os packet scaleway softlayer triton vsphere
            2019/12/13 12:39:53 [INFO] agent: Joining LAN cluster...
            2019/12/13 12:39:53 [INFO] agent: (LAN) joining: [192.168.50.10]
            2019/12/13 12:39:53 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.10:8301
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm0 192.168.50.10
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm2 192.168.50.12
            2019/12/13 12:39:53 [INFO] agent: (LAN) joined: 1
            2019/12/13 12:39:53 [DEBUG] agent: systemd notify failed: No socket
            Join completed. Synced with 1 initial agents
            2019/12/13 12:39:53 [INFO] agent: started state syncer
        ==> Consul agent running!
            2019/12/13 12:39:53 [INFO] consul: Adding LAN server vm0 (Addr: tcp/192.168.50.10:8300) (DC: dc1)
            2019/12/13 12:39:53 [ERR] consul: Member {vm0 192.168.50.10 8301 map[acls:0 bootstrap:1 build:1.6.2:1200f25e dc:dc1 id:14355616-ca8d-adfa-fc89-3f505b1db70b port:8300 raft_vsn:3 role:consul segment: vsn:2 vsn_max:3 vsn_min:2 wan_join_port:8302] alive 1 5 2 2 5 4} has bootstrap mode. Expect disabled.
            2019/12/13 12:39:53 [INFO] consul: Adding LAN server vm2 (Addr: tcp/192.168.50.12:8300) (DC: dc1)
            2019/12/13 12:39:53 [ERR] consul: Member {vm0 192.168.50.10 8301 map[acls:0 bootstrap:1 build:1.6.2:1200f25e dc:dc1 id:14355616-ca8d-adfa-fc89-3f505b1db70b port:8300 raft_vsn:3 role:consul segment: vsn:2 vsn_max:3 vsn_min:2 wan_join_port:8302] alive 1 5 2 2 5 4} has bootstrap mode. Expect disabled.
            2019/12/13 12:39:53 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.10:8301
            2019/12/13 12:39:53 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.10:8302
            2019/12/13 12:39:53 [DEBUG] raft-net: 192.168.50.11:8300 accepted connection from: 192.168.50.10:34490
            2019/12/13 12:39:53 [WARN]  raft: Failed to get previous log: 21 log not found (last: 0)
            2019/12/13 12:39:53 [DEBUG] memberlist: Stream connection from=192.168.50.10:50856
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm2.dc1 192.168.50.12
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm0.dc1 192.168.50.10
            2019/12/13 12:39:53 [INFO] consul: Handled member-join event for server "vm2.dc1" in area "wan"
            2019/12/13 12:39:53 [INFO] consul: Handled member-join event for server "vm0.dc1" in area "wan"
            2019/12/13 12:39:53 [DEBUG] consul: Successfully performed flood-join for "vm0" at 192.168.50.10:8302
            2019/12/13 12:39:53 [INFO] agent: (LAN) joined: 1
            2019/12/13 12:39:53 [DEBUG] agent: systemd notify failed: No socket
            2019/12/13 12:39:53 [INFO] agent: Join LAN completed. Synced with 1 initial agents
            2019/12/13 12:39:53 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8302
            2019/12/13 12:39:53 [DEBUG] consul: Successfully performed flood-join for "vm2" at 192.168.50.12:8302
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
            2019/12/13 12:39:54 [DEBUG] agent: Skipping remote check "serfHealth" since it is managed automatically
            2019/12/13 12:39:54 [INFO] agent: Synced node info
            2019/12/13 12:39:54 [DEBUG] agent: Node info in sync
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] raft-net: 192.168.50.11:8300 accepted connection from: 192.168.50.10:34492
            2019/12/13 12:39:55 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:55 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:55 [DEBUG] serf: messageJoinType: vm1.dc1
            
            
#  vm0 
    
        2019/12/13 12:38:56 [DEBUG] consul: Skipping self join check for "vm0" since the cluster is too small
            2019/12/13 12:39:04 [DEBUG] memberlist: Stream connection from=192.168.50.12:46034
            2019/12/13 12:39:05 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8301
            2019/12/13 12:39:20 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8302
            2019/12/13 12:39:25 [DEBUG] memberlist: Stream connection from=192.168.50.12:60102
            2019/12/13 12:39:35 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8301
            2019/12/13 12:39:43 [DEBUG] agent: Skipping remote check "serfHealth" since it is managed automatically
            2019/12/13 12:39:43 [DEBUG] agent: Node info in sync
            2019/12/13 12:39:47 [DEBUG] manager: Rebalanced 2 servers, next active server is vm0.dc1 (Addr: tcp/192.168.50.10:8300) (DC: dc1)
            2019/12/13 12:39:53 [DEBUG] memberlist: Stream connection from=192.168.50.11:43796
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm1 192.168.50.11
            2019/12/13 12:39:53 [INFO] consul: Adding LAN server vm1 (Addr: tcp/192.168.50.11:8300) (DC: dc1)
            2019/12/13 12:39:53 [INFO]  raft: Updating configuration with AddNonvoter (d4a52cdc-ec02-1653-16e9-600b9b65d5f5, 192.168.50.11:8300) to [{Suffrage:Voter ID:14355616-ca8d-adfa-fc89-3f505b1db70b Address:192.168.50.10:8300} {Suffrage:Voter ID:1601af2d-875f-a40f-9830-813eea971b3d Address:192.168.50.12:8300} {Suffrage:Nonvoter ID:d4a52cdc-ec02-1653-16e9-600b9b65d5f5 Address:192.168.50.11:8300}]
            2019/12/13 12:39:53 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.11:8302
            2019/12/13 12:39:53 [INFO]  raft: Added peer d4a52cdc-ec02-1653-16e9-600b9b65d5f5, starting replication
            2019/12/13 12:39:53 [DEBUG] memberlist: Stream connection from=192.168.50.11:35216
            2019/12/13 12:39:53 [DEBUG] memberlist: Stream connection from=192.168.50.11:43800
            2019/12/13 12:39:53 [INFO] consul: member 'vm1' joined, marking health alive
            2019/12/13 12:39:53 [INFO] serf: EventMemberJoin: vm1.dc1 192.168.50.11
            2019/12/13 12:39:53 [INFO] consul: Handled member-join event for server "vm1.dc1" in area "wan"
            2019/12/13 12:39:53 [WARN]  raft: AppendEntries to {Nonvoter d4a52cdc-ec02-1653-16e9-600b9b65d5f5 192.168.50.11:8300} rejected, sending older logs (next: 1)
            2019/12/13 12:39:53 [DEBUG] consul: Successfully performed flood-join for "vm1" at 192.168.50.11:8302
            2019/12/13 12:39:53 [INFO]  raft: pipelining replication to peer {Nonvoter d4a52cdc-ec02-1653-16e9-600b9b65d5f5 192.168.50.11:8300}
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:53 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm1.dc1
            2019/12/13 12:39:54 [DEBUG] serf: messageJoinType: vm0.dc1
            2019/12/13 12:39:54 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
            2019/12/13 12:39:55 [DEBUG] memberlist: Stream connection from=192.168.50.12:60104
            2019/12/13 12:40:04 [DEBUG] memberlist: Stream connection from=192.168.50.12:46040
            2019/12/13 12:40:05 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8301
            2019/12/13 12:40:06 [INFO] autopilot: Promoting Server (ID: "d4a52cdc-ec02-1653-16e9-600b9b65d5f5" Address: "192.168.50.11:8300") to voter
            2019/12/13 12:40:06 [INFO]  raft: Updating configuration with AddStaging (d4a52cdc-ec02-1653-16e9-600b9b65d5f5, 192.168.50.11:8300) to [{Suffrage:Voter ID:14355616-ca8d-adfa-fc89-3f505b1db70b Address:192.168.50.10:8300} {Suffrage:Voter ID:1601af2d-875f-a40f-9830-813eea971b3d Address:192.168.50.12:8300} {Suffrage:Voter ID:d4a52cdc-ec02-1653-16e9-600b9b65d5f5 Address:192.168.50.11:8300}]
            2019/12/13 12:40:20 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8302
            2019/12/13 12:40:25 [DEBUG] memberlist: Stream connection from=192.168.50.12:60108
            2019/12/13 12:40:35 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.12:8301
            2019/12/13 12:40:47 [DEBUG] memberlist: Stream connection from=192.168.50.11:43808

# start vm3 ( consul-client)

    vagrant@vm3:/vagrant$ sudo consul agent --config-dir vm3-consul_client.json
    ==> Starting Consul agent...
               Version: 'v1.6.2'
               Node ID: '734a76ec-a024-976c-f9d7-8502502890f1'
             Node name: 'vm3'
            Datacenter: 'dc1' (Segment: '')
                Server: false (Bootstrap: false)
           Client Addr: [127.0.0.1] (HTTP: 8500, HTTPS: -1, gRPC: -1, DNS: 8600)
          Cluster Addr: 192.168.50.13 (LAN: 8301, WAN: 8302)
               Encrypt: Gossip: true, TLS-Outgoing: false, TLS-Incoming: false, Auto-Encrypt-TLS: false
    
    ==> Log data will now stream in as it occurs:
    
        2019/12/13 13:14:58 [DEBUG] agent: Using random ID "734a76ec-a024-976c-f9d7-8502502890f1" as node ID
        2019/12/13 13:14:58 [DEBUG] tlsutil: Update with version 1
        2019/12/13 13:14:58 [INFO] serf: EventMemberJoin: vm3 192.168.50.13
        2019/12/13 13:14:58 [INFO] agent: Started DNS server 127.0.0.1:8600 (udp)
        2019/12/13 13:14:58 [INFO] agent: Started DNS server 127.0.0.1:8600 (tcp)
        2019/12/13 13:14:58 [INFO] agent: Started HTTP server on 127.0.0.1:8500 (tcp)
        2019/12/13 13:14:58 [INFO] agent: started state syncer
    ==> Consul agent running!
        2019/12/13 13:14:58 [INFO] agent: Retry join LAN is supported for: aliyun aws azure digitalocean gce k8s mdns os packet scaleway softlayer triton vsphere
        2019/12/13 13:14:58 [INFO] agent: Joining LAN cluster...
        2019/12/13 13:14:58 [INFO] agent: (LAN) joining: [192.168.50.10]
        2019/12/13 13:14:58 [WARN] manager: No servers available
        2019/12/13 13:14:58 [ERR] agent: failed to sync remote state: No known Consul servers
        2019/12/13 13:14:58 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.50.10:8301
        2019/12/13 13:14:58 [INFO] serf: EventMemberJoin: vm0 192.168.50.10
        2019/12/13 13:14:58 [INFO] serf: EventMemberJoin: vm1 192.168.50.11
        2019/12/13 13:14:58 [INFO] serf: EventMemberJoin: vm2 192.168.50.12
        2019/12/13 13:14:58 [INFO] agent: (LAN) joined: 1
        2019/12/13 13:14:58 [DEBUG] agent: systemd notify failed: No socket
        2019/12/13 13:14:58 [INFO] agent: Join LAN completed. Synced with 1 initial agents
        2019/12/13 13:14:58 [INFO] consul: adding server vm0 (Addr: tcp/192.168.50.10:8300) (DC: dc1)
        2019/12/13 13:14:58 [INFO] consul: adding server vm1 (Addr: tcp/192.168.50.11:8300) (DC: dc1)
        2019/12/13 13:14:58 [INFO] consul: adding server vm2 (Addr: tcp/192.168.50.12:8300) (DC: dc1)
        2019/12/13 13:14:59 [DEBUG] serf: messageJoinType: vm3
        2019/12/13 13:14:59 [DEBUG] serf: messageJoinType: vm3
        2019/12/13 13:14:59 [DEBUG] serf: messageJoinType: vm3
        2019/12/13 13:14:59 [DEBUG] serf: messageJoinType: vm3
        2019/12/13 13:15:01 [DEBUG] tlsutil: OutgoingRPCWrapper with version 1
        2019/12/13 13:15:01 [DEBUG] agent: Skipping remote check "serfHealth" since it is managed automatically
        2019/12/13 13:15:01 [INFO] agent: Synced node info
        2019/12/13 13:15:01 [DEBUG] agent: Node info in sync
        2019/12/13 13:15:02 [DEBUG] agent: Skipping remote check "serfHealth" since it is managed automatically
        2019/12/13 13:15:02 [DEBUG] agent: Node info in sync
        2019/12/13 13:15:05 [DEBUG] memberlist: Stream connection from=192.168.50.10:60326
        2019/12/13 13:15:26 [DEBUG] memberlist: Stream connection from=192.168.50.12:46714
        2019/12/13 13:15:35 [DEBUG] memberlist: Stream connection from=192.168.50.10:60330
        
        