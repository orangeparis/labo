
# building vagrant file

notes from : https://www.digitalocean.com/community/tutorials/how-to-configure-consul-in-a-production-environment-on-ubuntu-14-04



## script

    adduser consul
    
    mkdir -p /etc/consul.d/{bootstrap,server,client}
    
    mkdir /var/consul
    chown consul:consul /var/consul

    nano /etc/consul.d/bootstrap/config.json
    nano /etc/consul.d/server/config.json
    nano /etc/consul.d/client/config.json
    

## Create an Upstart Script

    nano /etc/init/consul.conf
    
### server content

    description "Consul server process"
    
    start on (local-filesystems and net-device-up IFACE=eth0)
    stop on runlevel [!12345]
    
    respawn
    
    setuid consul
    setgid consul
    
    exec consul agent -config-dir /etc/consul.d/server
    
### client content

    description "Consul client process"
    
    start on (local-filesystems and net-device-up IFACE=eth0)
    stop on runlevel [!12345]
    
    respawn
    
    setuid consul
    setgid consul
    
    exec consul agent -config-dir /etc/consul.d/client


## start the cluster

on the boostrap machine  (vm0)

    su consul
    consul agent -config-dir /etc/consul.d/bootstrap
    
on each server machine (vm1,vm2) as root:

    start consul 
    

## convert boostrap server to normal server

we now have a running consul cluster with a bootstrap and 2 server

we want to run bootstrap as a normal server

on the boostrap machine (vm0)

    CTRL+C
    
    exit
    start consul

we have now a fully functional cluster with 3 servers

## start the consul service on each client machine ( vm3 ..)

    start consul
    

## watch members

on any machine

    consul members
    

    