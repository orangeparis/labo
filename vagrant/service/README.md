
# useful info about systemd/upstart

    https://www.digitalocean.com/community/tutorials/how-to-configure-a-linux-service-to-start-automatically-after-a-crash-or-reboot-part-1-practical-examples
    
## systemd 
* debian 7 , 8
* ubuntu > 15.04
* centOS 7

## upstart 





# some config for systemd

    https://gist.github.com/yunano/c27eb679a29ab70178ca
    
    
    

# consul service sample 

    https://computingforgeeks.com/how-to-setup-consul-cluster-on-centos-rhel/
    
    
# another

    # Consul systemd service unit file
    [Unit]
    Description=Consul Service Discovery Agent
    Documentation=https://www.consul.io/
    After=network-online.target
    Wants=network-online.target
    
    [Service]
    Type=simple
    User=consul
    Group=consul
    ExecStart=/usr/local/bin/consul agent -server -ui \
    	-advertise=192.168.10.10 \
    	-bind=192.168.10.10 \
    	-data-dir=/var/lib/consul \
    	-node=consul-01 \
    	-config-dir=/etc/consul.d
    
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGINT
    TimeoutStopSec=5
    Restart=on-failure
    SyslogIdentifier=consul
    
    [Install]
    WantedBy=multi-user.target
    