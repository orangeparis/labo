
# vagrant

a space to set up the deployement infrastucture

consists of 3 vagrant vm based on /assets/Vagrantfile


* vm0 : 192.168.50.100
* vm1 : 192.168.50.101
* vm2 : 192.168.50.102



## start machines

    vagrant up bootstrap
    vagrant up vm1
    vagrant up vm2
    

on a terminal

    vagrant ssh bootstrap
    # start nomad server
    nomad agent -config files/server.bootstrap.hcl -bind 192.168.50.100
    
on another terminal

    vagrant ssh vm1
    nomad agent -config files/server.infra.hcl -bind 192.168.50.101
    
on another terminal

    vagrant ssh vm2
    nomad agent -config files/server.infra.hcl -bind 192.168.50.102
     

##  

now we should have a working nomad cluster on those 3 machines ( bootstrap,vm1,vm2)