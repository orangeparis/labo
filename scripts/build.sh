
export GOPATH=~/Go
export GOARCH=amd64
export CGO_ENABLED=0


echo "build for linux-amd64 in build/packages/linux_amd64"
export GOOS=linux

echo "    compile labo"
go build -o ../build/packages/${GOOS}_${GOARCH}/labo ../cmd/labo

# copy linux labo to vagrant
#echo "    copy labo to vagrant"
#cp ../build/packages/${GOOS}_${GOARCH}/labo ../vagrant/labo
