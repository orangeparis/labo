package web

import (
	"bitbucket.org/orangeparis/ines/heartbeat"
	"html/template"
	//"io/ioutil"
	"log"
	"net/http"
)

type Page struct{}

var delay int = 10
var templates template.Template

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func Serve(URL string, base string, natsServer string) {

	if URL == "" {
		URL = "0.0.0.0:3000"
	}
	if base == "" {
		base = "/tmp"
	}

	// handle /repository : fileserver
	HandleRepository(base)

	// handle /embeded
	RouteEmbeded()

	// handle /heartbeat/ and /metrics
	heartbeat.Setup(natsServer, delay)

	http.HandleFunc("/", HomeHandler)

	log.Println("Listening on %s ...", URL)
	_ = http.ListenAndServe(URL, nil)

}
