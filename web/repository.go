package web

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
)

func HandleRepository(base string) {

	if base == "" {
		base = "/tmp"
	}

	//fs := http.FileServer(http.Dir("/tmp"))

	fs := http.FileServer(http.Dir(base))

	http.Handle("/repository/", fs)
	//http.Handle("/repository/", http.StripPrefix("/repository/", fs))

	http.Handle("/static/", fs)
	//http.Handle("/static/", http.StripPrefix("/static/", fs))

	//func binaryHandler() (func ( w http.ResponseWriter, r *http.Request)) {
	//	binaryDir := path.Join(base, "repository", "bin")
	//
	//	return
	//
	//}
	http.Handle("/upload/bin", uploadHandler(base, "bin"))
	http.Handle("/upload/images", uploadHandler(base, "images"))

	http.HandleFunc("/upload", uploadFile)

}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	fmt.Println("File Upload Endpoint Hit")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := ioutil.TempFile("temp-images", "upload-*.png")
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)
	// return that we have successfully uploaded our file!
	fmt.Fprintf(w, "Successfully Uploaded File\n")
}

func uploadHandler(base string, kind string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Execute the query.

		targetDirectory := path.Join(base, "repository", kind)
		// check or create directory
		os.MkdirAll(targetDirectory, 0777)

		// Parse our multipart form, 10 << 20 specifies a maximum
		// upload of 10 MB files.
		r.ParseMultipartForm(10 << 20)
		// FormFile returns the first file for the given key `myFile`
		// it also returns the FileHeader so we can get the Filename,
		// the Header and the size of the file
		file, handler, err := r.FormFile("myFile")
		if err != nil {
			fmt.Println("Error Retrieving the File")
			fmt.Println(err)
			return
		}
		defer file.Close()
		fmt.Printf("Uploaded File: %+v\n", handler.Filename)
		fmt.Printf("File Size: %+v\n", handler.Size)
		fmt.Printf("MIME Header: %+v\n", handler.Header)

		// Create a temporary file within our temp-images directory that follows
		// a particular naming pattern
		name := path.Join(targetDirectory, handler.Filename)
		targetFile, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY, 0777)
		if err != nil {
			fmt.Println(err)
		}
		defer targetFile.Close()

		//tempFile, err := ioutil.TempFile("temp-images", "upload-*.png")
		//if err != nil {
		//	fmt.Println(err)
		//}
		//defer tempFile.Close()

		// read all of the contents of our uploaded file into a
		// byte array
		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err)
		}
		// write this byte array to our temporary file
		//tempFile.Write(fileBytes)
		targetFile.Write(fileBytes)
		// return that we have successfully uploaded our file!
		fmt.Fprintf(w, "Successfully Uploaded File to %s\n", name)

	})
}
