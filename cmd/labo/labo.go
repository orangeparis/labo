package main

import (
	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/ines/nping"
	"bitbucket.org/orangeparis/labo/system"
	"bitbucket.org/orangeparis/labo/system/consul"
	"bitbucket.org/orangeparis/labo/system/docker"
	"bitbucket.org/orangeparis/labo/system/nats"
	"bitbucket.org/orangeparis/labo/system/nomad"
	"bitbucket.org/orangeparis/labo/system/resgate"
	"bitbucket.org/orangeparis/labo/web"

	"log"

	//"bitbucket.org/orangeparis/labo/system/nats"
	"flag"
	"fmt"
	"os"
)

/*
	pre-requisites:

		/usr/bin/consul
		/usr/bin/nomad
		/usr/bin/nats-server


	nodes:

	* lab0  (bootstrap) consul_server , nomad_server , nats-server

	* lab1 lab2  ( servers)  consul_server , nomad server

	* lab n      ( worker )  consul client nomad client


	scp ./lab vagrant@192.168.50.12:lab

*/
const Version = "0.1.2"

const bootstrapNode = "lab0"
const defaultInterface = "eth1"
const envIface = "LABO_INTERFACE"

// return
func findIp(ipv4, iface string) (ip string, err error) {

	if ipv4 != "" {
		log.Printf("Explicit ipv4 choice: %s", ipv4)
		return ipv4, err
	}
	return ipv4FromIface(iface)
}

// retrieve ip from interface
func ipv4FromIface(iface string) (ip string, err error) {

	if iface == "" {
		iface = os.Getenv(envIface)
	}
	if iface == "" {
		iface = defaultInterface
	}
	ipv4, err := system.GetIpv4(iface)
	if err != nil {
		log.Printf("Failed to determine ip from iface %s : %s\n", iface, err.Error())
		return ip, err
	}
	ip = ipv4.String()
	log.Printf("IP determined for iface %s : %s\n", iface, ip)
	return ip, err
}

// configureBootstrap : lab0 as consul_server, nomad_server and nats server
func configureBootstrap(name string, ip string, region string, iface string) error {

	err := consul.ConfigureConsul("server", name, ip, "", true)
	if err != nil {
		return err
	}

	err = nomad.ConfigureNomad("server", ip, name, region, iface)
	if err != nil {
		return err
	}

	err = nats.ConfigureNats(ip)
	if err != nil {
		return err
	}

	err = resgate.Configure(ip)
	if err != nil {
		return err
	}

	err = docker.Install(ip)
	if err != nil {
		return err
	}

	return nil
}

// configureServer : lab1 ,lab2 as consul_server, nomad_server
func configureServer(name string, ip string, join string, region string, iface string) error {

	err := consul.ConfigureConsul("server", name, ip, join, false)
	if err != nil {
		return err
	}

	err = nomad.ConfigureNomad("server", ip, name, region, iface)
	if err != nil {
		return err
	}

	err = docker.Install(join)
	if err != nil {
		return err
	}

	return nil
}

// configureWorker  labn as consul_client nomad_client
func configureWorker(name string, ip string, join string, ui bool, region string, iface string) error {

	err := consul.ConfigureConsul("client", name, ip, join, ui)
	if err != nil {
		return err
	}

	err = nomad.ConfigureNomad("client", ip, name, region, iface)
	if err != nil {
		return err
	}

	err = docker.Install(join)
	if err != nil {
		return err
	}

	return nil
}

func main() {

	// configure-bootstrap (lab0)
	bootCmd := flag.NewFlagSet("configure-bootstrap", flag.ExitOnError)
	fip := bootCmd.String("ip", "", "specify ip node address")
	finterface := bootCmd.String("interface", "", "bind interface, default: eth1")
	fregion := bootCmd.String("region", "labo", "nomad region default:labo")

	// configure-node (lab1 - labn )
	nodeCmd := flag.NewFlagSet("configure-node", flag.ExitOnError)
	fNodeName := nodeCmd.String("name", "", "unique node name (lab1 ... labn)")
	fNodeIp := nodeCmd.String("ip", "", "specify ip node address")
	fNodeInterface := nodeCmd.String("interface", "", "bind interface, default: eth1")
	fNodeJoin := nodeCmd.String("join", "", "specify the bootstrap node ip address")
	fNodeUi := nodeCmd.Bool("ui", false, "add ui")
	fNodeRegion := nodeCmd.String("region", "labo", "nomad region default:labo")

	// labo web server ( starts labo server)
	serverCmd := flag.NewFlagSet("server", flag.ExitOnError)
	fServerNats := serverCmd.String("nats", "", "nats-server url")
	fServerBindAddress := serverCmd.String("bind", "0.0.0.0:3000", "web server bind address")
	fServerBaseDir := serverCmd.String("base", "/var/lib/labo", "base repository address")

	//fServerInterface := serverCmd.String("interface", "", "bind interface, default: eth1")

	// check)heartbeat
	checkCmd := flag.NewFlagSet("check-heartbeat", flag.ExitOnError)
	fCheckNats := checkCmd.String("nats", "", "nats-server url")
	fCheckService := checkCmd.String("service", "", "service with Heartbeat")
	fCheckTimeout := checkCmd.Int("timeout", 5, "timeout waiting for Heartbeat.<service>")

	// ping nats service
	pingCmd := flag.NewFlagSet("ping", flag.ExitOnError)
	fPingkNats := pingCmd.String("nats", "", "nats-server url")
	fPingService := pingCmd.String("service", "", "nats service  eg : lbservices")
	fPingTimeout := pingCmd.Int("timeout", 1, "timeout in second")

	// see :http://blog.ralch.com/tutorial/golang-subcommands/
	if len(os.Args) == 1 {
		fmt.Println("usage: labo <command> [<args>]")
		fmt.Println("The most commonly used labo commands are: ")
		fmt.Println("configure-bootstrap   Configure lab0 with consul,nomad, nats-server")
		fmt.Println("configure-node   Configure a node (lab1,lab2 as server,labn as client)")
		fmt.Println("server  launch the labo http server")
		fmt.Println("check-heartbeat  wait for Heartbeat.<service> message or timeout")
		fmt.Println("ping  a nats service")

		fmt.Println("version   show version of this script")
		return
	}

	// check user has root privilege
	//CheckSudo()

	switch os.Args[1] {
	case "configure-bootstrap":
		CheckSudo()
		_ = bootCmd.Parse(os.Args[2:])
	case "configure-node":
		CheckSudo()
		_ = nodeCmd.Parse(os.Args[2:])
	case "server":
		_ = serverCmd.Parse(os.Args[2:])
	case "check-heartbeat":
		_ = checkCmd.Parse(os.Args[2:])
	case "ping":
		_ = pingCmd.Parse(os.Args[2:])
	case "version":
		fmt.Printf("Version: %s", Version)
		os.Exit(0)
	default:
		fmt.Printf("%q is not valid command.\n", os.Args[1])
		os.Exit(2)
	}

	if bootCmd.Parsed() {
		// configure bootstrap lab0

		ip, err := findIp(*fip, *finterface)
		if err != nil {
			log.Printf("abort: cannot determine ip: %s\n", err.Error())
			os.Exit(2)
		}

		err = configureBootstrap(bootstrapNode, ip, *fregion, *finterface) // lab0
		if err != nil {
			os.Exit(2)
		}
		os.Exit(0)
	}

	if nodeCmd.Parsed() {
		// configure servers lab1 , lab2 , and clients labn

		ip, err := findIp(*fNodeIp, *fNodeInterface)
		if err != nil {
			log.Printf("abort: cannot determine ip: %s\n", err.Error())
			os.Exit(2)
		}

		if *fNodeName == "lab1" || *fNodeName == "lab2" {
			// this is a server
			err = configureServer(*fNodeName, ip, *fNodeJoin, *fNodeRegion, *fNodeInterface)

		} else {
			// this is a worker
			err = configureWorker(*fNodeName, ip, *fNodeJoin, *fNodeUi, *fNodeRegion, *fNodeInterface)

		}
		if err != nil {
			os.Exit(2)
		}
		os.Exit(0)
	}

	if serverCmd.Parsed() {
		// start labo server
		var natsServer = *fServerNats // "nats://127.0.0.1:4222"
		var URL = *fServerBindAddress //"0.0.0.0:3000"
		var base = *fServerBaseDir    // "/var/lib/labo"

		// launch labo web server
		go web.Serve(URL, base, natsServer)

		log.Printf("starts labo web server at: %s\n", URL)
		// wait forever
		select {}

	}

	if checkCmd.Parsed() {

		ok, err := heartbeat.CheckHeartbeat(*fCheckNats, *fCheckService, *fCheckTimeout)
		if err != nil {
			os.Exit(2)
		}
		if ok == true {
			// OK
			os.Exit(0)
		}
		os.Exit(1)
	}

	if pingCmd.Parsed() {
		fmt.Printf("pinging service: [%s] ...\n", *fPingService)
		c, err := nping.NewClient(*fPingkNats)
		if err != nil {
			fmt.Printf("ping failed to connect: %s\n", err.Error())
			os.Exit(2)
		}

		ok, err := c.Ping(*fPingService, *fPingTimeout)
		if ok == true {
			// ping ok
			fmt.Printf("PING service: %s OK\n", *fPingService)
			os.Exit(0)
		} else {
			fmt.Printf("failed to ping service: [%s] \n", *fPingService)
			os.Exit(2)
		}

	}

}

func CheckSudo() {
	// check effective user id is 0 ( root or via sudo )
	userId := os.Geteuid()
	if userId != 0 {
		fmt.Printf("Abort: Need root privileges")
		os.Exit(2)
	}

}
