package main

import (
	"bitbucket.org/orangeparis/labo/web"
)

var URL = "0.0.0.0:3000"

//var base = "/tmp"
//var base = "/Users/cocoon/Go/src/bitbucket.org/orangeparis/labo/assets"
var base = "/var/lib/labo"

var natsServer = "nats://127.0.0.1:4222"

func main() {

	go web.Serve(URL, base, natsServer)

	// wait forever
	select {}

}
