# This declares a job named "docs". There can be exactly one
# job declaration per job file.
job "lbservices" {
  # Specify this job should run in the region named "us". Regions
  # are defined by the Nomad servers' configuration.
  #region = "labo"

  # Spread the tasks in this job between us-west-1 and us-east-1.
  datacenters = ["dc1"]

  # Run this job as a "service" type. Each job type has different
  # properties. See the documentation below for more examples.
  type = "service"

  # Specify this job to have rolling updates, two-at-a-time, with
  # 30 second intervals.
  update {
    stagger      = "30s"
    max_parallel = 2
  }

  # A group defines a series of tasks that should be co-located
  # on the same client (host). All tasks within a group will be
  # placed on the same host.
  group "lbservices" {
    # Specify the number of these tasks we want.
    count = 1

    # Create an individual task (unit of work). This particular
    # task utilizes a Docker container to front a web application.
    task "lbservices" {

      constraint {
        attribute = "${node.unique.name}"
        value = "lb0"
      }

      driver = "exec"
      config {
        command = "/var/lib/repository/bin/lbservices"
        args = [
          "--nats",
          "192.168.50.10:4222",
        ]

        # The service block tells Nomad how to register this service
        # with Consul for service discovery and monitoring.
        service {
          # This tells Consul to monitor the service on the port
          # labelled "http". Since Nomad allocates high dynamic port
          # numbers, we use labels to refer to them.
          #port = "http"

          check {
            id = "api"
            name =  "Labo Heartbeat"
            http = "http://192.168.50.10:3000/heartbeat/lbservices"
            #"tls_skip_verify": false,
            method = "GET"
            interval = "10s"
            timeout = "1s"
          }

        }

      }
      # It is possible to set environment variables which will be
      # available to the task when it runs.
      #env {
      #  "DB_HOST" = "db01.example.com"
      #  "DB_USER" = "web"
      #  "DB_PASS" = "loremipsum"
      #}

      # Specify the maximum resources required to run the task,
      # include CPU, memory, and bandwidth.
      resources {
        cpu    = 500 # MHz
        memory = 128 # MB

        network {
          mbits = 100

          # This requests a dynamic port named "http". This will
          # be something like "46283", but we refer to it via the
          # label "http".
          //port "http" {}

          # This requests a static port on 443 on the host. This
          # will restrict this task to running once per host, since
          # there is only one port 443 on each host.
          #port "https" {
          #  static = 443
          #}
        }
      }
    }
  }
}
