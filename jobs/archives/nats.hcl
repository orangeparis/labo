job "nats" {
  datacenters = [
    "dc1"]

  group "standalone" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    task "nats" {

      constraint {
        attribute = "${node.unique.name}"
        value = "vm3"
      }

      driver = "exec"
      config {
        command = "/usr/bin/nats-server"
        args = [
          "--addr", "192.168.50.13",
          "--client_advertise", "192.168.50.13",
          "-m" , "8222"
        ]

        port_map {
          client = "192.168.50.13:4222"
          monitoring = "192.168.50.13:8222"
          routing = "192.168.50.13:6222"
        }
      }

      resources {
        cpu = 400
        # 100 MHz
        memory = 128
        # 128MB

        network {
          mbits = 1

          port "client" {
            static = 4222
          }

          port "monitoring" {
            static = 8222
          }

          port "routing" {
            static = 6222
          }
        }
      }

      service {
        port = "client"
        name = "nats"
        tags = [
          "faas"]

        check {
          type = "http"
          port = "monitoring"
          path = "/connz"
          interval = "5s"
          timeout = "2s"
        }
      }
    }
  }
}
