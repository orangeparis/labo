job "nats-cluster" {
  # All tasks in this job must run on linux.
  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }
  datacenters = ["dc1"]
  type ="service"

  group "nats" {
    # All groups in this job should be scheduled on different hosts.
    constraint {
      operator  = "distinct_hosts"
      value     = "true"
    }

    task "nats-server1" {
      constraint {
        attribute  = "${node.unique.name}"
        value     = "bootstrap"
      }
      driver = "docker"
      config {
        image = "hashicorp/http-echo"
        args  = ["-text", "hello","-listen" ,":8080"]
      }

      service {
        # This tells Consul to monitor the service on the port
        # labelled "http". Since Nomad allocates high dynamic port
        # numbers, we use labels to refer to them.
        port = "http"
        check {
          type     = "http"
          path     = "/"
          interval = "10s"
          timeout  = "2s"
        }
      }
      resources {
        cpu    = 500 # MHz
        memory = 128 # MB

        network {
          mbits = 100

          # This requests a dynamic port named "http". This will
          # be something like "46283", but we refer to it via the
          # label "http".
          port "http" {
            static = 8080
          }
        }
      }

      env {
        "DB_HOST" = "db01.example.com"
        "DB_USER" = "web"
        "DB_PASS" = "loremipsum"
      }

    }

    task "nats-server2" {
      constraint {
        attribute  = "${node.unique.name}"
        value     = "server1"
      }
      driver = "docker"
      config {
        image = "hashicorp/http-echo"
        args  = ["-text", "hello"]
      }
    }

    task "nats-server3" {
      constraint {
        attribute  = "${node.unique.name}"
        value     = "server2"
      }
      driver = "docker"
      config {
        image = "hashicorp/http-echo"
        args  = ["-text", "hello"]
      }
    }
  }
}