package resgate

import (
	"bitbucket.org/orangeparis/ines/systemd"
	//"bitbucket.org/orangeparis/labo/_systemd"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strconv"

	//"fmt"
	"log"
	"os"

	//"os"
	"os/user"
)

/*

	configure and install resgate

	eg resgate --nats nats://192.168.50.10:4222 --addr 192.168.50.10 --port 8080 --reqtimeout 3000


	resgate must be on the same machine than nats

*/

//const ResgateExecutable = "/usr/bin/resgate"
//const ResgateCnfDirectory = "/etc/resgate.d"
//const ResgateCnfFile = "/etc/resgate.d/config.json"
//const ResgateConsulServiceFilename = "/etc/consul.d/resgate-service.json"

var serviceName = "resgate"
var username = "consul"
var port = "8080"

type Defaults struct {
	DataDir  string
	LogLevel string

	Executable            string // "/usr/bin/<service>"
	CnfDirectory          string // "/etc/<service>.d"
	CnfFile               string //  "/etc/<service>.d/config.json"
	ConsulServiceFilename string // "/etc/consul.d/<service>-service.json"
}

var defaults = Defaults{
	DataDir:  "/var/lib/resgate",
	LogLevel: "INFO",

	Executable:            "/usr/bin/resgate",
	CnfDirectory:          "/etc/resgate.d",
	CnfFile:               "/etc/resgate.d/config.json",
	ConsulServiceFilename: "/etc/consul.d/resgate-service.json",
}

var Parameters = map[string]interface{}{

	"data_dir":  defaults.DataDir,
	"log_level": defaults.LogLevel,
}

func Configure(ip string) error {

	// generate configuration for a Node
	log.Printf("Generate %s configuration", serviceName)
	cnf, _ := Configuration(ip)

	// install configuration
	installer := &Installer{}
	err := installer.setupUser(username)
	if err != nil {
		log.Printf("user dont exists: %s", username)
		return err
	}

	src, _ := json.Marshal(cnf)
	dst := &bytes.Buffer{}
	if err := json.Indent(dst, src, "", "  "); err != nil {
	}
	log.Println(defaults.CnfFile)
	log.Printf(dst.String())

	err = installer.Install(dst.Bytes())
	if err != nil {
		log.Printf("Failed to install :%s\n", err.Error())
		return err
	}
	log.Printf("Configuration successfully installed \n")

	// install systemd  service
	log.Println("setup systemd service")

	// resgate --nats nats://192.168.50.10:4222 --addr 192.168.50.10 --port 8080 --reqtimeout 3000
	args := ""
	for k, v := range cnf {
		args += fmt.Sprintf("--%s %s ", k, v)
	}

	service, err := systemd.NewSystemdService(
		serviceName,
		serviceName,
		username,            // "consul"
		defaults.Executable, //"/usr/bin/<service>",
		args,
	)
	if err != nil {
		log.Printf("Failed to setup systemd service :%s\n", err.Error())
		return err
	}

	err = service.Install()
	if err != nil {
		log.Printf("Failed to install systemd service :%s\n", err.Error())
		return err
	}
	log.Printf("systemd service successfully installed\n")

	log.Printf("start and enable service\n")
	err = service.Setup()
	if err != nil {
		log.Printf("failed to start and enable service\n")
		log.Printf("to manually start service: sudo systemctl start %s", serviceName)
		log.Printf("to manually enable service: sudo systemctl enable %s", serviceName)
	} else {
		log.Printf("systemd service successfully started and enabled\n")
	}

	//
	// install consul service
	//
	s, _ := NewConsulService(ip, port, defaults.ConsulServiceFilename) //  "/etc/consul.d/resgate-service.json")
	err = s.Install()

	return err

}

func Configuration(ip string) (cnf map[string]interface{}, err error) {

	// resgate --nats nats://192.168.50.10:4222 --addr 192.168.50.10 --port 8080 --reqtimeout 3000
	cnf = make(map[string]interface{})
	cnf["nats"] = fmt.Sprintf("nats://%s:4222", ip)
	cnf["addr"] = ip
	cnf["port"] = port
	cnf["reqtimeout"] = "300"
	return cnf, err
}

type Installer struct {
	User *user.User
	// consul uid gid
	Uid int
	Gid int
}

//
func (c *Installer) Install(content []byte) error {

	if c.checkSudo() == false {
		err := errors.New("abort: needs root privilege")
		return err
	}

	// create /etc/<service>.d
	log.Printf("create %s\n", defaults.CnfDirectory)
	err := c.mkdir(defaults.CnfDirectory)
	if err != nil {
		log.Printf("Install failed :%s", err.Error())
	}
	// create /var/lib/<service>
	log.Printf("create %s\n", defaults.DataDir)
	err = c.mkdir(defaults.DataDir)
	if err != nil {
		log.Printf("Install failed :%s", err.Error())
	}

	// install file /etc/<service>.d/config.json
	log.Printf("create %s\n", defaults.CnfFile)
	err = ioutil.WriteFile(defaults.CnfFile, content, 0666)
	if err == nil {
		err = os.Chown(defaults.CnfFile, c.Uid, c.Gid)
	}
	if err != nil {
		log.Printf("install failed: %s", err.Error())
		return err
	}

	// check consul executable  /usr/bin/consul ?
	if _, err := os.Stat(defaults.Executable); os.IsNotExist(err) {
		log.Printf("cannot find executable [%s]: %s", defaults.Executable, err.Error())
		return err
	}
	err = os.Chown(defaults.Executable, c.Uid, c.Gid)
	return err

}

func (c *Installer) checkSudo() bool {
	if os.Geteuid() != 0 {
		return false
	}
	return true
}

func (c *Installer) mkdir(path string) error {

	if _, err := os.Stat(path); err != nil {
		err := os.Mkdir(path, 0777)
		if err != nil {
			return err
		}
	}
	err := os.Chown(path, c.Uid, c.Gid)
	return err
}

func (c *Installer) setupUser(username string) error {

	// load user
	u, err := user.Lookup(username)
	if err == nil {
		// user ok sett it
		c.User = u
		c.Uid, _ = strconv.Atoi(u.Uid)
		c.Gid, _ = strconv.Atoi(u.Gid)
	}
	return err
}
