package resgate

import (
	"bitbucket.org/orangeparis/labo/system"
	"bytes"
	"io/ioutil"
	"log"
	"os/exec"
	"text/template"
)

/*

	register resgate as a consul service

	usage :

		s,_ := NewConsulService("192.168.50.10" ,"/etc/consul.d./resgate-service.json"
		err := s.Install()


*/

var consulServiceTemplate = `
{
  "service": {
    "name": "resgate",
    "tags": [
      "resgate"
    ],
    "port": {{.Port}},
    "tagged_addresses": {
      "lan": {
        "address": "{{.Ipv4}}",
        "port": {{.Port}}
      }
    },
    "check": {
      "id": "resgate",
      "name": "resgate",
      "tcp": "{{.Ipv4}}:{{.Port}}",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}

`

const ConsulServiceFilename = "etc/consul.d/resgate-service.json"
const DefaultPort = "8080"

type ConsulService struct {
	Path string // etc/consul.d/resgate-service.json
	Ipv4 string
	Port string // 8080
}

func NewConsulService(ipv4 string, port string, path string) (service *ConsulService, err error) {

	if path == "" {
		// set default to "/etc/consul.d/resgate-service.json"
		path = ConsulServiceFilename
	}
	if port == "" {
		port = DefaultPort
	}
	service = &ConsulService{Ipv4: ipv4, Port: port, Path: path}
	return service, nil

}

func (s *ConsulService) Content() (text string, err error) {

	tmpl, err := template.New("resgate").Parse(consulServiceTemplate)
	if err != nil {
		log.Println("Failed to parse consul service template")
		return text, err
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, s)
	if err != nil {
		log.Printf("Failed to execute template: %s\n" + err.Error())
		return text, err
	}
	text = buf.String()
	return text, nil

}

// Install gen and create file /etc/nats-server.d/nats-service.json
func (s *ConsulService) Install() error {

	// gen content
	text, err := s.Content()
	if err != nil {
		return err
	}

	// write file
	err = ioutil.WriteFile(s.Path, []byte(text), 0666)
	if err != nil {
		log.Printf("cannot create file %s: %s \n", s.Path, err.Error())
		return err
	}

	// chown consul ?
	err = s.chown()
	if err != nil {
		return err
	}

	// reload consul
	err = ReloadConsul()

	return nil
}

func (s *ConsulService) chown() error {

	user, err := system.LookupUser("consul")
	if err != nil {
		// consul user does not exists
		return err
	}
	// change owner of service file to "consul"
	err = user.Chown(s.Path)
	return err

}

// ReloadConsul execute shell command "consul reload")
func ReloadConsul() error {

	cmd := exec.Command("consul", "reload")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	log.Printf("consul reload: %q\n", out.String())
	if err != nil {
		log.Printf("consul reload failed %s\n%q\n", err.Error(), out.String())
		return err
	}
	return nil
}
