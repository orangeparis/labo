# resgate.io


    see: https://resgate.io
    
    serviceName  resgate
    port         8080
    
    
## sample start

    resgate --nats nats://192.168.50.10:4222 --addr 192.168.50.10 --port 8080 --reqtimeout 3000
    
    
## files

	Executable:            "/usr/bin/resgate",
	CnfDirectory:          "/etc/resgate.d",
	CnfFile:               "/etc/resgate.d/config.json",
	ConsulServiceFilename: "/etc/consul.d/resgate-service.json",
	
	
