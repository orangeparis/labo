package system

import (
	"fmt"
	"testing"
)

/*

	needs root privillege to run it

	cd $home/Go/src/bitbucket.org/orangeparis/labo/
	sudo /usr/local/bin/goland .


*/

func TestIterIface(t *testing.T) {

	iterIface()

}

func TestGetIpv4(t *testing.T) {

	ip, err := GetIpv4("en0")
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		t.Fail()
		return
	}
	fmt.Printf("ip: %s\n", ip.String())

	//  test no ipv4
	ip, err = GetIpv4("en1")
	if err != nil {
		fmt.Printf("%s\n", err.Error())
	}

	// test inexistant interface
	ip, err = GetIpv4("inexsistant")
	if err != nil {
		fmt.Printf("%s\n", err.Error())
	}

}
