package nats

import (
	"bitbucket.org/orangeparis/labo/system"
	"bytes"
	"io/ioutil"
	"log"
	"os/exec"
	"text/template"
)

/*

	register nats-server as a consul service

	usage :

		s,_ := NewConsulService("192.168.50.10" ,"/etc/consul.d/nats-service.json"
		err := s.Install()


*/

var consulServiceTemplate = `
{
  "service": {
    "name": "nats",
    "tags": [
      "nats-server"
    ],
    "port": 4222,
    "tagged_addresses": {
      "lan": {
        "address": "{{.Ipv4}}",
        "port": 4222
      }
    },
    "check": {
      "id": "nats",
      "name": "nats_connz",
      "http": "http://{{.Ipv4}}:8222/connz",
      "method": "GET",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
`

type ConsulService struct {
	Path string // etc/nats-server.d/nats-service.json
	Ipv4 string
}

func NewConsulService(ipv4 string, path string) (service *ConsulService, err error) {

	if path == "" {
		// set default to "/etc/nats-server.d/nats-service.json"
		path = NatsConsulServiceFilename
	}
	service = &ConsulService{Ipv4: ipv4, Path: path}

	return service, nil

}

func (s *ConsulService) Content() (text string, err error) {

	tmpl, err := template.New("nats-server").Parse(consulServiceTemplate)
	if err != nil {
		log.Println("Failed to parse consul service template")
		return text, err
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, s)
	if err != nil {
		log.Printf("Failed to execute template: %s\n" + err.Error())
		return text, err
	}
	text = buf.String()
	return text, nil

}

// Install gen and create file /etc/nats-server.d/nats-service.json
func (s *ConsulService) Install() error {

	// gen content
	text, err := s.Content()
	if err != nil {
		return err
	}

	// write file
	err = ioutil.WriteFile(s.Path, []byte(text), 0666)
	if err != nil {
		log.Printf("cannot create file %s: %s \n", s.Path, err.Error())
		return err
	}

	// chown consul ?
	err = s.chown()
	if err != nil {
		return err
	}

	// reload consul
	err = ReloadConsul()

	return nil
}

func (s *ConsulService) chown() error {

	user, err := system.LookupUser("consul")
	if err != nil {
		// consul user does not exists
		return err
	}
	// change owner of service file to "consul"
	err = user.Chown(s.Path)
	return err

}

// ReloadConsul execute shell command "consul reload")
func ReloadConsul() error {

	cmd := exec.Command("consul reload")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	log.Printf("consul reload: %q\n", out.String())
	if err != nil {
		log.Printf("consul reload failed %s\n%q\n", err.Error(), out.String())
		return err
	}

	return nil
}
