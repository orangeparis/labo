configuring a nats cluster ( and supercluster )



## configure a gateway

    gateway {
        name: "A"
        listen: "localhost:7222"
        authorization {
            user: gwu
            password: gwp
        }
    
        gateways: [
            {name: "A", url: "nats://gwu:gwp@localhost:7222"},
            {name: "B", url: "nats://gwu:gwp@localhost:7333"},
            {name: "C", url: "nats://gwu:gwp@localhost:7444"},
        ]
    }
    
## configure a service 

start consul agent with

    /usr/bin/consul agent -config-file=/etc/consul.d/config.json -enable-script-checks -config-dir=/etc/consul.d
 
 
 create file  /etc/consul.d/nats-service.json
 
    {
      "service": {
        "name": "nats",
        "tags": [
          "nats-server"
        ],
        "tagged_addresses": {
          "lan": {
            "address": "192.168.50.10",
            "port": 4222
          }
        },
        "check": {
          "id": "nats",
          "name": "nats connz",
          "http": "https://192.168.50.10:8222/connz",
          "method": "GET",
          "interval": "10s",
          "timeout": "1s"
        }
      }
    }
 