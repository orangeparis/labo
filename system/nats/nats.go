package nats

import (
	"bitbucket.org/orangeparis/ines/shell"
	"bitbucket.org/orangeparis/ines/systemd"
	//"bitbucket.org/orangeparis/labo/systemd"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strconv"

	//"fmt"
	"log"
	"os"

	//"os"
	"os/user"
)

var NatsExecutable = "/usr/bin/nats-server"

const NatsCnfDirectory = "/etc/nats-server.d"
const NatsCnfFile = "/etc/nats-server.d/config.json"
const NatsConsulServiceFilename = "/etc/consul.d/nats-service.json"

type Defaults struct {
	DataDir  string
	LogLevel string
}

var defaults = Defaults{
	DataDir:  "/var/lib/nats-server",
	LogLevel: "INFO",
}

var NatsBase = map[string]interface{}{

	"data_dir":  defaults.DataDir,
	"log_level": defaults.LogLevel,
}

func ConfigureNats(ip string) error {

	// check nats-server executable
	natsPath, err := shell.LocateBin("nats-server")
	if err != nil {
		log.Println("configureNats failed: missing nats-server executable ")
		// update global
		NatsExecutable = natsPath
	}

	service_name := "nats-server"

	// generate nats configuration for a Node
	log.Printf("Generate nats configuration")
	cnf, _ := NatsConfiguration(ip)

	//
	// install configuration
	//
	installer := &NatsInstaller{}
	installer.setupUser("consul")

	src, _ := json.Marshal(cnf)
	dst := &bytes.Buffer{}
	if err := json.Indent(dst, src, "", "  "); err != nil {
	}
	log.Printf(dst.String())

	err = installer.Install(dst.Bytes())
	if err != nil {
		log.Printf("Failed to install :%s\n", err.Error())
		return err
	}
	log.Printf("Configuration successfully installed \n")

	//
	// install systemd  service
	//
	log.Println("setup systemd service")
	args := fmt.Sprintf("--addr %s --client_advertise %s -m %d",
		cnf["--addr"],
		cnf["--client_advertise"],
		cnf["-m"],
	)
	service, err := systemd.NewSystemdService(
		"nats-server",
		"nats server",
		"consul",
		natsPath,
		args,
	)
	if err != nil {
		log.Printf("Failed to setup systemd service :%s\n", err.Error())
		return err
	}

	err = service.Install()
	if err != nil {
		log.Printf("Failed to install systemd service :%s\n", err.Error())
		return err
	}
	log.Printf("systemd service successfully installed\n")

	log.Printf("start and enable service\n")
	err = service.Setup()
	if err != nil {
		log.Printf("failed to start and enable service\n")
		log.Printf("to manually start service: sudo systemctl start %s", service_name)
		log.Printf("to manually enable service: sudo systemctl enable %s", service_name)
	} else {
		log.Printf("systemd service successfully started and enabled\n")
	}

	//
	// install consul service
	//
	s, _ := NewConsulService(ip, NatsConsulServiceFilename) //  "/etc/consul.d/nats-service.json")
	err = s.Install()

	return err

}

func NatsConfiguration(ip string) (cnf map[string]interface{}, err error) {

	cnf = make(map[string]interface{})
	cnf["--addr"] = ip
	cnf["--client_advertise"] = ip
	cnf["-m"] = 8222
	return cnf, err
}

type NatsInstaller struct {
	User *user.User
	// consul uid gid
	Uid int
	Gid int
}

//
func (c *NatsInstaller) Install(content []byte) error {

	if c.checkSudo() == false {
		err := errors.New("abort: needs root privilege")
		return err
	}

	// create /etc/<service>.d
	log.Printf("create %s\n", NatsCnfDirectory)
	err := c.mkdir(NatsCnfDirectory)
	if err != nil {
		log.Printf("Consul Install failed :%s", err.Error())
	}
	// create /var/lib/<service>
	log.Printf("create %s\n", defaults.DataDir)
	err = c.mkdir(defaults.DataDir)
	if err != nil {
		log.Printf("Install failed :%s", err.Error())
	}

	// install file /etc/<service>.d/config.json
	log.Printf("create %s\n", NatsCnfFile)
	err = ioutil.WriteFile(NatsCnfFile, content, 0666)
	if err == nil {
		err = os.Chown(NatsCnfFile, c.Uid, c.Gid)
	}
	if err != nil {
		log.Printf("install failed: %s", err.Error())
		return err
	}

	// check consul executable  /usr/bin/consul ?
	if _, err := os.Stat(NatsExecutable); os.IsNotExist(err) {
		log.Printf("install failed: %s", err.Error())
		return err
	}
	err = os.Chown(NatsExecutable, c.Uid, c.Gid)
	return err

}

func (c *NatsInstaller) checkSudo() bool {
	if os.Geteuid() != 0 {
		return false
	}
	return true
}

func (c *NatsInstaller) mkdir(path string) error {

	if _, err := os.Stat(path); err != nil {
		err := os.Mkdir(path, 0777)
		if err != nil {
			return err
		}
	}
	err := os.Chown(path, c.Uid, c.Gid)
	return err
}

func (c *NatsInstaller) setupUser(username string) error {

	// command:  sudo adduser --disabled-password --gecos "" consul
	_, err := user.Lookup(username)
	if err != nil {
		// user not existent : create it
		cmd := exec.Command("adduser", "--disabled-password", "--gecos", "", username)
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		if err != nil {
			log.Println(err.Error())
			return err
		}
		log.Printf("user consul created: %q\n", out.String())

	}
	// load user
	u, err := user.Lookup(username)
	if err == nil {
		// user ok sett it
		c.User = u
		c.Uid, _ = strconv.Atoi(u.Uid)
		c.Gid, _ = strconv.Atoi(u.Gid)
	}
	return err
}
