package system

import (
	"errors"
	"fmt"
	"log"
	"net"
)

func iterIface() {

	ifaces, err := net.Interfaces()
	// handle err
	if err != nil {
		log.Printf("abort: %s\n", err.Error())
		return
	}
	for _, i := range ifaces {
		log.Printf("=== Interface : %s\n", i.Name)
		addrs, err := i.Addrs()
		if err != nil {
			log.Printf("skip on : %s", err.Error())
			continue
		}
		if i.Name != "en0" {
			continue
		}

		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
				if ip.To4() != nil {
					fmt.Println(ip.String())
				}
			case *net.IPAddr:
				ip = v.IP
				//fmt.Println(ip.String())
			}
			// process IP address
		}
	}
}

func GetIpv4(iface string) (ip net.IP, err error) {

	ifaces, err := net.Interfaces()
	if err != nil {
		log.Printf("ERROR: %s\n", err.Error())
		return ip, err
	}

	gotIface := false

	for _, i := range ifaces {

		if i.Name != iface {
			continue
		}
		gotIface = true

		addrs, err := i.Addrs()
		if err != nil {
			log.Printf("ERROR: %s", err.Error())
			return ip, err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
				if ip.To4() != nil {
					return ip, nil
				}
			case *net.IPAddr:
				ip = v.IP
				//fmt.Println(ip.String())
			}
		}
	}
	if gotIface == false {
		// the interface was not found
		err = errors.New("No such interface: " + iface)
	} else {
		err = errors.New("No Ipv4 found")
	}
	return ip, err
}
