package consul

import (
	"bitbucket.org/orangeparis/labo/systemd"
	"encoding/json"
	"log"
	"testing"
)

func TestConsulServerConfiguration(t *testing.T) {

	ip := "192.168.50.1"
	name := "lab1"
	join := "192.168.50.10"
	ui := true

	cnf, err := ConsulConfiguration("server", name, ip, join, ui)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	if cnf["server"] != true {
		t.Fail()
		return
	}

	if cnf["advertise_addr"] != ip {
		t.Fail()
		return
	}

	if cnf["node_name"] != name {
		t.Fail()
		return
	}
	if cnf["ui"] != ui {
		t.Fail()
		return
	}

	rjoin := cnf["retry_join"].([]string)
	if len(rjoin) <= 0 {
		t.Fail()
		return
	}
	if rjoin[0] != join {
		t.Fail()
		return
	}

	j, err := json.MarshalIndent(cnf, "", "  ")
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	println(string(j))

}

func TestConsulClientConfiguration(t *testing.T) {

	ip := "192.168.50.1"
	name := "lab1"
	join := "192.168.50.10"
	ui := true

	cnf, err := ConsulConfiguration("client", name, ip, join, ui)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	if cnf["server"] != false {
		t.Fail()
		return
	}

	if cnf["advertise_addr"] != ip {
		t.Fail()
		return
	}

	if cnf["node_name"] != name {
		t.Fail()
		return
	}
	if cnf["ui"] != ui {
		t.Fail()
		return
	}

	rjoin := cnf["retry_join"].([]string)
	if len(rjoin) <= 0 {
		t.Fail()
		return
	}
	if rjoin[0] != join {
		t.Fail()
		return
	}

	j, err := json.MarshalIndent(cnf, "", "  ")
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	println(string(j))

}

func TestConsulSystemdConfiguration(t *testing.T) {

	// check nomad executable
	consulPath := "/usr/bin/consul"

	service, err := _systemd.NewSystemdService(
		"consul",
		"consul cluster",
		"consul",
		consulPath,
		" agent -config-file=/etc/consul.d/config.json -enable-script-checks -config-dir=/etc/consul.d")
	if err != nil {
		log.Printf("Failed to setup consul systemd service :%s\n", err.Error())
		t.Fail()
		return
	}

	j, err := json.MarshalIndent(service, "", "  ")
	if err != nil {
		t.Fatalf("%s\n", err)
		return
	}
	println(string(j))

}
