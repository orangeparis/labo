package consul

import (
	"bitbucket.org/orangeparis/ines/shell"
	"bitbucket.org/orangeparis/ines/systemd"
	//"bitbucket.org/orangeparis/labo/systemd"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strconv"

	//"fmt"
	"log"
	"os"

	//"os"
	"os/user"
)

/*

 */

var ConsulExecutable = "/usr/bin/consul"

const ConsulCnfDirectory = "/etc/consul.d"
const ConsulCnfFile = "/etc/consul.d/config.json"

type Defaults struct {
	Datacenter         string
	DataDir            string
	LogLevel           string
	EnableSyslog       bool
	Encrypt            string
	RetryInterval      string
	BootstrapExpect    int
	Interface          string
	EnableScriptChecks bool
	Recursors          []string
}

var defaults = Defaults{
	Datacenter:         "dc1",
	DataDir:            "/var/lib/consul",
	LogLevel:           "INFO",
	EnableSyslog:       true,
	Encrypt:            "7i+WtYw9q8Sc6eTrIA1iQjHUiEDFnGKKbq9/NtrwKqU=",
	RetryInterval:      "15s",
	BootstrapExpect:    3,
	Interface:          "eth1",
	EnableScriptChecks: true,
	Recursors:          []string{"127.0.0.1:53"},
}

var consulServerBase = map[string]interface{}{

	"datacenter":           defaults.Datacenter,
	"data_dir":             defaults.DataDir,
	"log_level":            defaults.LogLevel,
	"enable_syslog":        defaults.EnableSyslog,
	"encrypt":              defaults.Encrypt,
	"rejoin_after_leave":   true,
	"bootstrap":            false,
	"server":               true,
	"retry_interval":       defaults.RetryInterval,
	"bootstrap_expect":     defaults.BootstrapExpect,
	"enable_script_checks": defaults.EnableScriptChecks,
	"recursors":            defaults.Recursors,

	//"advertise_addr": "192.168.50.13",
	//"retry_join":  []string{},
}

var consulClientBase = map[string]interface{}{

	"datacenter":         defaults.Datacenter,
	"data_dir":           defaults.DataDir,
	"log_level":          defaults.LogLevel,
	"enable_syslog":      defaults.EnableSyslog,
	"encrypt":            defaults.Encrypt,
	"retry_interval":     defaults.RetryInterval,
	"rejoin_after_leave": true,
	"bootstrap":          false,
	"server":             false,
	"recursors":          defaults.Recursors,

	// "advertise_addr": "192.168.50.13",
	//"retry_join":  []string{},

}

func ConfigureConsul(mode string, name string, ip string, join string, ui bool) error {

	// check nomad executable
	consulPath, err := shell.LocateBin("consul")
	if err != nil {
		log.Println("ConfigureConsul failed: missing consul executable ")
		// update global
		ConsulExecutable = consulPath
	}

	// generate consul configuration for a Node
	log.Printf("Generate consul configuration")
	cnf, _ := ConsulConfiguration(mode, name, ip, join, ui)
	//log.Printf("\n%s: %v\n", mode, cnf)

	// install consul configuration
	installer := &ConsulInstaller{}
	src, _ := json.Marshal(cnf)
	dst := &bytes.Buffer{}
	if err := json.Indent(dst, src, "", "  "); err != nil {
	}
	log.Printf(dst.String())

	err = installer.Install(dst.Bytes())
	if err != nil {
		log.Printf("Failed to install :%s\n", err.Error())
		return err
	}
	log.Printf("Consul Configuration successfully installed \n")

	// install dnsmasq configuration
	//err = ConfigureDnsmasq(ip)
	//if err != nil {
	//	log.Printf("Cannot configure dnsmask: %s", err.Error())
	//}

	// install systemd consul service
	log.Println("setup consul systemd service")
	service, err := systemd.NewSystemdService(
		"consul",
		"consul cluster",
		"consul",
		consulPath,
		" agent -config-file=/etc/consul.d/config.json -enable-script-checks -config-dir=/etc/consul.d")
	if err != nil {
		log.Printf("Failed to setup consul systemd service :%s\n", err.Error())
		return err
	}

	err = service.Install()
	if err != nil {
		log.Printf("Failed to install consul systemd service :%s\n", err.Error())
		return err
	}
	log.Printf("consul systemd service successfully installed\n")

	log.Printf("start and enable consul service\n")

	// start and enable service
	err = service.Setup()
	if err != nil {
		log.Printf("failed to start and enable consul service\n")
	} else {
		log.Printf("consul systemd service successfully started and enabled\n")
	}
	log.Println("to manually start service: sudo systemctl start consul")
	log.Println("to manually enable service: sudo systemctl enable consul")

	return err

}

func ConsulConfiguration(mode string, name string, ip string, join string, ui bool) (cnf map[string]interface{}, err error) {

	cnf = make(map[string]interface{})
	switch mode {

	case "server":
		cnf = consulServerBase

	case "client":
		cnf = consulClientBase
	default:
		log.Printf("ConsulConfiguration: invalid mode: %s", mode)
		return cnf, err
	}
	if name != "" {
		cnf["node_name"] = name
	}
	if ip != "" {
		cnf["advertise_addr"] = ip
	} else {
		// no ip given take ipv4 address of the named interface  ( eg eth1 )
		cnf["advertise_addr"] = fmt.Sprintf(`{{ GetInterfaceIP "%s" }}`, defaults.Interface)
	}
	if join != "" {
		cnf["retry_join"] = []string{join}
	}
	if ui == true {
		// sudo consul agent --ui --client 0.0.0.0 --config-dir vm3-consul_client.json
		cnf["ui"] = true
		cnf["client_addr"] = "0.0.0.0"
	}
	return cnf, err
}

type ConsulInstaller struct {
	User *user.User
	// consul uid gid
	Uid int
	Gid int
}

// content : content of config.json
func (c *ConsulInstaller) Install(content []byte) error {

	if c.checkSudo() == false {
		err := errors.New("abort: needs root privilege")
		return err
	}

	// check consul user exists or create it
	err := c.setupUser()
	if err != nil {
		err = errors.New("no consul user")
		return err
	}

	// create /etc/consul.d
	log.Printf("create %s\n", ConsulCnfDirectory)
	err = c.mkdir(ConsulCnfDirectory)
	if err != nil {
		log.Printf("Consul Install failed :%s", err.Error())
	}
	// create /var/lib/consul
	log.Printf("create %s\n", defaults.DataDir)
	err = c.mkdir(defaults.DataDir)
	if err != nil {
		log.Printf("Consul Install failed :%s", err.Error())
	}

	// install file /etc/consul.d/config.json
	log.Printf("create %s\n", ConsulCnfFile)
	err = ioutil.WriteFile(ConsulCnfFile, content, 0666)
	if err == nil {
		err = os.Chown(ConsulCnfFile, c.Uid, c.Gid)
	}
	if err != nil {
		log.Printf("consul install failed: %s", err.Error())
		return err
	}

	// check consul executable  /usr/bin/consul ?
	if _, err := os.Stat(ConsulExecutable); os.IsNotExist(err) {
		log.Printf("consul install failed: %s", err.Error())
		return err
	}
	err = os.Chown(ConsulExecutable, c.Uid, c.Gid)
	return err

}

func (c *ConsulInstaller) checkSudo() bool {
	if os.Geteuid() != 0 {
		return false
	}
	return true
}

func (c *ConsulInstaller) mkdir(path string) error {

	if _, err := os.Stat(path); err != nil {
		err := os.Mkdir(path, 0777)
		if err != nil {
			return err
		}
	}
	err := os.Chown(path, c.Uid, c.Gid)
	return err
}

func (c *ConsulInstaller) setupUser() error {

	// command:  sudo adduser --disabled-password --gecos "" consul
	_, err := user.Lookup("consul")
	if err != nil {
		// user not existent : create it
		cmd := exec.Command("adduser", "--disabled-password", "--gecos", "", "consul")
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		if err != nil {
			log.Println(err.Error())
			return err
		}
		log.Printf("user consul created: %q\n", out.String())
		err = c.AddToGroup("docker")
		if err != nil {
			log.Println(err.Error())
			return err
		}
		log.Printf("user consul added to docker group: %q\n", out.String())
	}
	// load user
	u, err := user.Lookup("consul")
	if err == nil {
		// user ok sett it
		c.User = u
		c.Uid, _ = strconv.Atoi(u.Uid)
		c.Gid, _ = strconv.Atoi(u.Gid)
	}
	return err
}

// add user to a group
func (c *ConsulInstaller) AddToGroup(group string) error {

	log.Printf("add user consul to group %s\n", group)
	// sudo gpasswd -a consul docker
	cmd := exec.Command("gpasswd", "-a", "consul", group)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return err
}

// dnsmask configure  : echo "server=/consul/127.0.0.1#8600" > /etc/dnsmasq.d/10-consul
// so we can go "dig lab0.node.consul"
func ConfigureDnsmasq(ip string) (err error) {

	if ip == "" {
		ip = "127.0.0.1"
	}

	line := fmt.Sprintf("server=/consul/%s/#8600\n", ip)
	err = ioutil.WriteFile("/etc/dnsmasq.d/10-consul", []byte(line), 0666)
	return
}
