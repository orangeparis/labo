package system

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/user"
	"strconv"
)

/*

	manage users on linux


	# add a user to docker group
	sudo gpasswd -a consul docker


	# for nomad client
	export NOMAD_ADDR=http://192.168.50.13:4646

	usage:
		user := &system.User{}
		_ := user.SetupUser( "consul" )
		user.AddToGroup("docker")




*/

type User struct {
	*user.User
	Uid int
	Gid int
}

func LookupUser(username string) (u *User, err error) {
	x, err := user.Lookup(username)
	if err == nil {
		// user ok set it
		Uid, _ := strconv.Atoi(x.Uid)
		Gid, _ := strconv.Atoi(x.Uid)
		u = &User{x, Uid, Gid}
	}
	return u, err
}

func (u *User) SetupUser(name string) error {

	// command:  sudo adduser --disabled-password --gecos "" consul
	u.Username = name
	_, err := user.Lookup(name)
	if err != nil {
		// user not existent : create it
		cmd := exec.Command("adduser", "--disabled-password", "--gecos", "", name)
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		if err != nil {
			log.Println(err.Error())
			return err
		}
		fmt.Printf("user %s created: %q\n", name, out.String())
	}
	// load user
	x, err := user.Lookup(name)
	if err == nil {
		// user ok sett it
		u.User = x
		u.Uid, _ = strconv.Atoi(x.Uid)
		u.Gid, _ = strconv.Atoi(x.Gid)
	}
	return err
}

// add user to a group
func (u *User) AddToGroup(group string) error {

	log.Printf("add user [%s] to docker group\n", u.Username)
	// sudo gpasswd -a consul docker
	cmd := exec.Command("gpasswd", "-a", u.Username, group)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return err
}

// chown file
func (u *User) Chown(filename string) error {
	return os.Chown(filename, u.Uid, u.Gid)
}
