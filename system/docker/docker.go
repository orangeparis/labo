package docker

import (
	"fmt"
	"io/ioutil"
	"log"
)

/*

	configure docker


	/etc/docker/daemon.json  : enable insecure registry at lab0.node.consul:5000

*/

var DaemonFilename = "/etc/docker/daemon.json"
var Port = 5000

func DaemonConfiguration(registryIP string) (content string, err error) {

	// enable insecure registry at <ip>:5000
	// { "insecure-registries":["192.168.50.10:5000"] }
	//
	content = fmt.Sprintf(`{ "insecure-registries":["%d:%d"] }`, registryIP, Port)
	return
}

func Install(registryIP string) (err error) {

	log.Printf("create %s\n", DaemonFilename)
	content, _ := DaemonConfiguration(registryIP)
	err = ioutil.WriteFile(DaemonFilename, []byte(content), 0666)
	if err != nil {
		log.Printf("docker install failed: %s", err.Error())
		return
	}
	return
}
