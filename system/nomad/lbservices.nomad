#
# the lbservices nats service
#
job "lbservices" {
  datacenters = [
    "dc1"]

  group "lbservices" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    task "lbservices" {

      constraint {
        attribute = "${node.unique.name}"
        value = "lab0"
      }

#      artifact {
#        source = "http//192.168.50.10/repository/bin/lbservices"
#      }

      driver = "raw_exec"
      config {
        command = "/var/lib/labo/repository/bin/lbservices"
        args = [
          "--nats", "nats://192.168.50.10:4222"
        ]

      }

      resources {
        cpu = 400
        # 100 MHz
        memory = 128
        # 128MB

        network {
          mbits = 1
          }
      }
      service {
        name = "lbservices"
        tags = ["nats"]

        check {
          type     = "script"
          name     = "nping_lbservices"
          command  = "/usr/bin/labo"
          args     = ["ping","","--nats","nats://192.168.50:10:4222","--service","lbservices.dora"]
          interval = "60s"
          timeout  = "1s"

          check_restart {
            limit = 3
            grace = "10s"
            ignore_warnings = false
          }
        }

      }

      }
  }
}

