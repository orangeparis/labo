#
# the labo web server ( ui / repository / metrics )
# tested OK
job "labo" {
  datacenters = [
    "dc1"]

  group "labo" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    task "labo" {

      constraint {
        attribute = "${node.unique.name}"
        value = "lab0"
      }

      driver = "raw_exec"
      config {
        command = "/usr/bin/labo"
        args = [
          "server", "",
          "--nats", "nats://192.168.50.10:4222",
          "--bind" , "192.168.50.10:3000"
        ]

      }

      resources {
        cpu = 400
        # 100 MHz
        memory = 128
        # 128MB

        network {
          mbits = 1

          port "ui" {
            static = 3000
          }
        }
      }

      service {
        port = "ui"
        name = "uilab"
        tags = [
          "labo","ui"]

        check {
          type = "http"
          port = "ui"
          path = "/"
          interval = "5s"
          timeout = "2s"
        }
      }
    }
  }
}
