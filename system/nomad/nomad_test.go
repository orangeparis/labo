package nomad

import (
	"bitbucket.org/orangeparis/ines/systemd"
	"encoding/json"
	"log"
	"testing"
)

func TestNomadServerConfiguration(t *testing.T) {

	ip := "192.168.50.1"
	name := "labx"
	region := "ines"
	iface := "eth0"

	cnf, err := NomadConfiguration("server", ip, name, region, iface)
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	addresses := cnf["addresses"].(map[string]interface{})
	if addresses["http"] != ip {
		t.Fatalf("%s\n", err)
	}

	client := cnf["client"].(map[string]interface{})
	if client["network_interface"] != iface {
		t.Fatalf("%s\n", err)
	}

	j, err := json.MarshalIndent(cnf, "", "  ")
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	println(string(j))

}

func TestNomadClientConfiguration(t *testing.T) {

	ip := "192.168.50.1"
	name := "labx"
	region := "ines"
	iface := "eth0"

	cnf, err := NomadConfiguration("client", ip, name, region, iface)
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	addresses := cnf["addresses"].(map[string]interface{})
	if addresses["http"] != ip {
		t.Fatalf("%s\n", err)
	}

	client := cnf["client"].(map[string]interface{})
	if client["network_interface"] != iface {
		t.Fatalf("%s\n", err)
	}

	j, err := json.MarshalIndent(cnf, "", "  ")
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	println(string(j))

}

func TestNomadSystemdConfiguration(t *testing.T) {

	// check nomad executable
	nomadPath := "/usr/bin/nomad"

	service, err := systemd.NewSystemdService(
		"nomad",
		"nomad cluster",
		"root",
		nomadPath,
		"agent -config /etc/nomad.d/config.json")
	if err != nil {
		log.Printf("Failed to setup nomad systemd service :%s\n", err.Error())
		t.Fatal(err)
	}

	j, err := json.MarshalIndent(service, "", "  ")
	if err != nil {
		t.Fatalf("%s\n", err)
	}
	println(string(j))

}
