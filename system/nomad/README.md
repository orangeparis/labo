# nomad configuration tool


## minimal job configuration

    see: https://medium.com/@obenaus.thomas/a-good-default-nomad-job-template-ea448b8a8cdd
    
    
    
## processus

    make build →  docker build
    make push →  push image to artifactory
    make nomad.job →  render job file from template (if applicable)
    make deploy →  nomad run
    make stop →  nomad stop
    make status →  nomad status
    make logs →  nomad logs
    
    
 