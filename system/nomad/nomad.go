package nomad

import (
	"bitbucket.org/orangeparis/ines/shell"
	"bitbucket.org/orangeparis/ines/systemd"
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	//"fmt"
	"log"
	"os"

	//"os"
	"os/user"
)

var NomadExecutable = "/usr/bin/nomad"

const NomadCnfDirectory = "/etc/nomad.d"
const NomadCnfFile = "/etc/nomad.d/config.json"

type NomadDefaults struct {
	Region          string
	DataDir         string
	LogLevel        string
	EnableDebug     bool
	BootstrapExpect int
	BindAddr        string
	Datacenter      string
	Interface       string
	DriverWhitelist string
}

var nomadDefaults = NomadDefaults{
	Region:          "labo",
	DataDir:         "/var/lib/nomad",
	LogLevel:        "INFO",
	EnableDebug:     true,
	BootstrapExpect: 3,
	BindAddr:        "127.0.0.1",
	Datacenter:      "dc1",
	Interface:       "eth1",
	DriverWhitelist: "docker,qemu,exec,raw_exec",
}

var nomadConsulConfiguration = map[string]interface{}{
	//Consult agent's HTTP Address
	"address": "127.0.0.1:8500",
	// The service name to register the server and client with Consul.
	"server_service_name": "nomad",
	"client_service_name": "nomad-client",
	//# Auth info for http access
	//auth = user:password

	// Advertise Nomad services to Consul
	// Enables automatically registering the services
	"auto_advertise": true,
	// Enables the servers and clients bootstrap using Consul
	"server_auto_join": true,
	"client_auto_join": true,
}

// a server + client agent
var nomadServerBase = map[string]interface{}{

	"region":       nomadDefaults.Region,
	"data_dir":     nomadDefaults.DataDir,
	"log_level":    nomadDefaults.LogLevel,
	"enable_debug": nomadDefaults.EnableDebug,
	"bind_addr":    nomadDefaults.BindAddr,

	"server": map[string]interface{}{
		"enabled":          true,
		"bootstrap_expect": nomadDefaults.BootstrapExpect,
	},
	"client": map[string]interface{}{
		"enabled":           true,
		"network_interface": nomadDefaults.Interface,
		"options": map[string]interface{}{
			"driver.whitelist":       nomadDefaults.DriverWhitelist,
			"driver.raw_exec.enable": "1",
		},
	},
	"consul": nomadConsulConfiguration,
}

// a client only server
var nomadClientBase = map[string]interface{}{

	"region":       nomadDefaults.Region,
	"data_dir":     nomadDefaults.DataDir,
	"log_level":    nomadDefaults.LogLevel,
	"enable_debug": nomadDefaults.EnableDebug,
	"bind_addr":    nomadDefaults.BindAddr,

	"datacenter": nomadDefaults.Datacenter,
	"client": map[string]interface{}{
		"enabled":           true,
		"network_interface": nomadDefaults.Interface,
		"options": map[string]interface{}{
			"driver.whitelist":       nomadDefaults.DriverWhitelist,
			"driver.raw_exec.enable": "1",
		},
	},
	"consul": nomadConsulConfiguration,
}

// ConfigureNomad : generates config and install it
func ConfigureNomad(mode string, ip string, name string, region string, iface string) error {

	if region == "" {
		region = nomadDefaults.Region
	}
	if iface == "" {
		iface = nomadDefaults.Interface // eth1
	}

	// check nomad executable
	nomadPath, err := shell.LocateBin("nomad")
	if err != nil {
		log.Println("configureNomad failed: missing nomad executable ")
		// update global
		NomadExecutable = nomadPath
	}

	// generate nomad configuration for a Node
	log.Printf("Generate nomad configuration")
	cnf, _ := NomadConfiguration(mode, ip, name, region, iface)
	//log.Printf("\n%s: %v\n", mode, cnf)

	// install nomad configuration
	installer := &NomadInstaller{}
	src, _ := json.Marshal(cnf)
	dst := &bytes.Buffer{}
	if err := json.Indent(dst, src, "", "  "); err != nil {
		log.Printf(dst.String())
	}

	err = installer.Install(dst.Bytes())
	if err != nil {
		log.Printf("Failed to install :%s\n", err.Error())
		return err
	}
	log.Printf("Nomad Configuration successfully installed \n")

	// install systemd nomad service
	log.Println("setup nomad systemd service")
	service, err := systemd.NewSystemdService(
		"nomad",
		"nomad cluster",
		"root",
		nomadPath,
		"agent -config /etc/nomad.d/config.json")
	if err != nil {
		log.Printf("Failed to setup nomad systemd service :%s\n", err.Error())
		return err
	}

	err = service.Install()
	if err != nil {
		log.Printf("Failed to install nomad systemd service :%s\n", err.Error())
		return err
	}
	log.Printf("nomad systemd service successfully installed\n")

	log.Printf("start and enable nomad service\n")
	err = service.Setup()
	if err != nil {
		log.Printf("failed to start and nomad service\n")
	} else {
		log.Printf("nomad systemd service successfully started and enabled\n")
	}
	log.Println("to manually start service: sudo systemctl start nomad")
	log.Println("to manually enable service: sudo systemctl enable nomad")

	return err

}

func NomadConfiguration(mode string, ip string, name string, region string, iface string) (cnf map[string]interface{}, err error) {

	cnf = make(map[string]interface{})
	switch mode {

	case "server":
		cnf = nomadServerBase

	case "client":
		cnf = nomadClientBase
	default:
		log.Printf("NomadConfiguration: invalid mode: %s", mode)
		return cnf, err
	}
	// configure interface
	client := cnf["client"].(map[string]interface{})
	client["network_interface"] = iface

	if ip != "" {
		// Set the bind address
		cnf["addresses"] = map[string]interface{}{
			"http": ip,
			"rpc":  ip,
			"serf": ip,
		}
		cnf["advertise"] = map[string]interface{}{
			"http": ip + ":4646",
			"rpc":  ip + ":4647",
			"serf": ip + ":4648",
		}
	}

	if name != "" {
		// specify a unique name for the node ( default is hostname )
		cnf["name"] = name
	}
	if region != "" {
		// specify a unique name for the node ( default is hostname )
		cnf["region"] = region
	}
	return cnf, err
}

type NomadInstaller struct {
	User *user.User
	// consul uid gid
	Uid int
	Gid int
}

// content : content of config.json
func (c *NomadInstaller) Install(content []byte) error {

	if c.checkSudo() == false {
		err := errors.New("abort: needs root privilege")
		return err
	}

	// create /etc/nomad.d
	log.Printf("create %s\n", NomadCnfDirectory)
	err := c.mkdir(NomadCnfDirectory)
	if err != nil {
		log.Printf("nomad Install failed :%s", err.Error())
	}
	// create /var/lib/nomad
	log.Printf("create %s\n", nomadDefaults.DataDir)
	err = c.mkdir(nomadDefaults.DataDir)
	if err != nil {
		log.Printf("nomad Install failed :%s", err.Error())
	}

	// install file /etc/nomad.d/config.json
	log.Printf("create %s\n", NomadCnfFile)
	err = ioutil.WriteFile(NomadCnfFile, content, 0666)
	if err != nil {
		log.Printf("nomad install failed: %s", err.Error())
		return err
	}

	// check consul executable  /usr/bin/nomad ?
	_, err = shell.LocateBin("nomad")
	if err != nil {
		log.Printf("nomad install failed: cannot find nomad executable")
		return err
	}

	return err

}

func (c *NomadInstaller) checkSudo() bool {
	if os.Geteuid() != 0 {
		return false
	}
	return true
}

func (c *NomadInstaller) mkdir(path string) (err error) {

	if _, err = os.Stat(path); err != nil {
		err = os.Mkdir(path, 0777)
	}
	return err
}
