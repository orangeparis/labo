# labo

a project to deploy ines labo tools with nomad ( and consul )

see nomad doc:

    https://learn.hashicorp.com/nomad/getting-started/install
    
    


# tools

we need to deploy several tools

* nats server
* sniffer/netscan
* lbservices
* resgate io


# infrastructure

we got 5 vm with same image ( see vagrantfile: ubuntu with nomad and docker)

* vm0 :  lab0 ,the bootstrap machine ( nomad server + consul server + vm0-client)
* vm1 :  lab1 ,nomad server + nomad client vm1-client
* vm2 :  lab2 ,nomad server + nomad client vm2-client
* vm3 :  lab3 ,nomad client + consul cilent
* vm4 :  lab4, nomad client / consul client
* vm5 :  sniffer ,nomad client :  a special machine with access to NE network traffic to run sniffer


## profiles
all the vm comes from the same vm template be will be customized in 3 profiles

* infra:       vm0 : the boostrap machine for nomad and consul
* infra :     usually vm1 and vm2 , nomad and consul servers for qorum
* worker : the other machines vm3 .... without nomad-servers
 


# groups (datacenters)

we got 1 groups of vm (or datacenters)

*  dc1



# deployement process

1) initialize cluster ( nomad + consul )

## connect to lab0  ( boostrap eg 192.168.50.10)

    sudo labo configure-bootstrap --interface eth1 --region ines

## connect to lab1

    sudo labo configure-node --name lab1 --interface eth1 --region ines --join 192.168.50.10

## connect to lab2

    sudo labo configure-node --name lab2 --interface eth1 --region ines --join 192.168.50.10

## check cluster established

from a browser connect to consul ui    <vm0>:8500  eg  192.168.50.10:8500
check all services are ok

* consul        (3)
* nats          (2)
* nomad         (18)
* nomad-client  (6)
* resgate       (2)

from a browser connect to nomad ui    <vm0>:4646  eg 192.168.50.10:4646

* check we have 3 clients
* check we have 3 servers
