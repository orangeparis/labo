# server.infra.hcl
# a nomad server for profile:infra (  for vm1 and vm2 )
#   will try to join bootstrap at 192.168.1.100


# Increase log verbosity
log_level = "DEBUG"

# Setup data dir
data_dir = "/tmp/server"

# Enable the server
server {
    enabled = true

    # Self-elect, should be 3 or 5 for production
    bootstrap_expect = 3

    # This is the IP address of the first server we provisioned
    server_join {
        #retry_join = ["192.168.50.100:4648"]
        retry_join = ["boostrap.local:4648"]
    }

}
