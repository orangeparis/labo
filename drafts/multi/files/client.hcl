#
# client.hcl
# a client connecting to bootstrap nomad server ( for vm3,vm4 ..... )
#

# Increase log verbosity
log_level = "DEBUG"

# Setup data dir
data_dir = "/tmp/client"

# Give the agent a unique name. Defaults to hostname
#name = "vm0-client"

# Enable the client
client {
  enabled = true

  # For demo assume we are talking to server1. For production,
  # this should be like "nomad.service.consul:4647" and a system
  # like Consul used for service discovery.
  #servers = ["192.168.50.100:4647"]
  servers = ["bootstrap.local:4647"]
}

# Modify our port to be the same for all clients
ports {
  http = 5656
}