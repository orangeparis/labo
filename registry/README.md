# local docker registry

see: https://docs.docker.com/registry/deploying/



# sample usage

## start a local registry 

    docker run -d -p 5000:5000
    
## get an example image from docker hub and push it to the repository

    docker pull hello-world
    docker tag hello-world:latest localhost:5000/hello
    
    docker image remove hello-world:latest
    docker image remove localhost:5000/hello
    
 ## get image from local repository
 
    docker pull localhost:5000/hello
    
 # configure storage
 
     $ docker run -d \
       -p 5000:5000 \
       --restart=always \
       --name registry \
       -v /mnt/registry:/var/lib/registry \
       registry:2
       
   